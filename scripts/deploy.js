const hre = require("hardhat");
const fs = require('fs');

async function main() {
  /*const NFTMarket = await hre.ethers.getContractFactory("Marketplace");
  const nftMarket = await NFTMarket.deploy();
  await nftMarket.deployed();*/
  console.log("nftMarketplace deployed to:");

  const NFT = await hre.ethers.getContractFactory("Collection");
  const nft = await NFT.deploy("0x4977D048FdfcD73946ECD843C7B07f727EB109cC","Omnispaces Templates","OST");
  await nft.deployed();
  console.log("nft deployed to:", nft.address);
 /* const NFT2 = await hre.ethers.getContractFactory("BAFC_Omnispaces");
  const nft2 = await NFT2.deploy(nftMarket.address,"OMNISPACES","OMNI");
  await nft2.deployed();
  console.log("nft2 deployed to:", nft2.address);

  const Semifungible = await hre.ethers.getContractFactory("semifungible");
  const semifungible = await Semifungible.deploy(nftMarket.address,"Omnispaces","OMNI");
  await semifungible.deployed();
  console.log("semifungible deployed to:", semifungible.address);

  let config = `
  export const nftmarketaddress = "${nftMarket.address}"
  export const collection = "${nft.address}"
  `

  let data = JSON.stringify(config)
  fs.writeFileSync('config.js', JSON.parse(data))*/
}

main()
  .then(() => process.exit(0))
  .catch(error => {
    console.error(error);
    process.exit(1);
  });