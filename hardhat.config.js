require('@nomiclabs/hardhat-waffle')
require('dotenv').config()
require("@nomiclabs/hardhat-ethers");
const { API_URL, PRIVATE_KEY } = process.env;

module.exports = {
  solidity: "0.8.4",
  paths: {
    artifacts: './src/artifacts',
  },
  defaultNetwork: "matic",
  networks: {
      hardhat: {
      },
      matic: {
         url: API_URL,
         accounts: [`0x${PRIVATE_KEY}`],
        
      }
  },

};