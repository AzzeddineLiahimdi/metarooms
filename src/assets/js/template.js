const chatbox = jQuery.noConflict();

function chatboxOpen(){
  chatbox(".chatbox-popup, .chatbox-close").fadeIn()
}
function chatboxClose(){
  console.log("chatboxClose")
  chatbox(".chatbox-popup, .chatbox-close").fadeOut()
}
function chatboxMaximize(){
  chatbox(".chatbox-popup, .chatbox-open, .chatbox-close").fadeOut();
  chatbox(".chatbox-panel").fadeIn();
  chatbox(".chatbox-panel").css({ display: "flex" });
}
function chatboxMinimize(){
  chatbox(".chatbox-panel").fadeOut();
  chatbox(".chatbox-popup, .chatbox-open, .chatbox-close").fadeIn();
}
function chatboxPanelClose(){
  chatbox(".chatbox-panel").fadeOut();
    chatbox(".chatbox-open").fadeIn();
}

function hide(){
  console.log("hello")
}
