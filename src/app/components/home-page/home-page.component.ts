import { Component, OnDestroy, OnInit } from '@angular/core';
import { ViewportScroller } from '@angular/common';
import { PaginationService } from 'src/app/services/firebase-services/pagination.service';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.scss']
})

export class HomePageComponent implements OnInit, OnDestroy {
  public category!: string | null
  public template!: string | null
  public executed = false;
  constructor(
    private viewportScroller: ViewportScroller,
    public page: PaginationService
  ) {
    this.category = null
    this.template = null
  }

  ngOnInit() {
    this.page.init('rooms', 'createdAt', { reverse: true, value2: this.template, value1: this.category, limit: 8 })
  }

  ngOnDestroy(): void {
    this.page.sub.unsubscribe()
  }

  filterByCategory(cat: string | null) {
    this.page.sub.unsubscribe()
    this.category = cat
    this.page.init('rooms', 'createdAt', { value2: this.template, value1: this.category })
  }

  filterByTemplate(temp: string | null) {
    this.page.sub.unsubscribe()
    this.template = temp
    this.page.init('rooms', 'createdAt', { value2: this.template, value1: this.category })
  }

  public onClick(elementId: string): void {
    this.viewportScroller.scrollToAnchor(elementId);
  }
  alert() {
    alert("this feature is not yet integrated")
  }
  LoadMore() {
    this.page.more()
  }
}
