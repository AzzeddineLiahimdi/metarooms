import { Component, OnInit } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { Template } from 'src/app/models/templates';
import { ContractsService } from 'src/app/services/contracts.service';
import { TemplateService } from 'src/app/services/firebase-services/template.service';
import { IpfsService } from 'src/app/services/ipfs.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-create-template',
  templateUrl: './create-template.component.html',
  styleUrls: ['./create-template.component.scss']
})
export class CreateTemplateComponent implements OnInit {
  
 public image=""
 public ipfsImage=""
 public video=""
 public template=new Template()
  constructor(private ipfs: IpfsService,
    private tempService:TemplateService,
    private spinnerService: NgxSpinnerService,
    private contractService:ContractsService) { }

  ngOnInit(): void {
  }
  public async showImage(eventTarget: any) {
    if(eventTarget.files){
      var reader= new FileReader()
      reader.readAsDataURL(eventTarget.files[0])
      this.ipfsImage=eventTarget.files[0]
      reader.onload=(event:any)=>{
        this.image=event.target.result
      }
    }
  }
  async uploadVideo(eventTarget: any){
    this.video=eventTarget.files[0]
  }

  async onSubmit() {
    if(this.ipfsImage!= "" && this.template.name && this.template.description){
     // const cid=await this.ipfs.storPicture(this.ipfsImage)
      //this.template.image= "https://nftstorage.link/ipfs/"+cid
      //
      this.spinnerService.show()
      const cid=await this.ipfs.storPicture(this.video)
    const link= "ipfs://"+cid
      let [metadata,image] = await this.ipfs.storeAsset(this.template.name, this.template.description, this.ipfsImage,link,this.template.seneName);
      console.log("Metadata",metadata)
      let tokenId=await this.contractService.mintTemplate(this.template.amount,metadata,1000)
      if(tokenId) this.template.tokenId=tokenId;
      this.template.image=image
      this.template.contractAddress=environment.TEMPLATE8CONTRACT
      this.tempService.create(this.template)
      this.spinnerService.hide()
    console.log(this.template,metadata)

    }else{alert(3) }
  }

}
