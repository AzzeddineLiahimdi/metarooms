import { getNftMetadata, initializeAlchemy, Network } from '@alch/alchemy-sdk';
import { ChangeDetectorRef, Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { environment } from 'src/environments/environment';
import { OrderService } from 'src/app/services/firebase-services/order.service';
import { LazymintingService } from 'src/app/services/lazyminting.service';
import { DomSanitizer } from '@angular/platform-browser';
import { NgxSpinnerService } from 'ngx-spinner';
import { User } from 'src/app/models/user';
import { UserService } from 'src/app/services/firebase-services/user.service';
import { ContractsService } from 'src/app/services/contracts.service';
import { Subscription } from 'rxjs';
import { AuthService } from 'src/app/services/auth.service';
import Swal from 'sweetalert2';

@Component({
	selector: 'app-asset',
	templateUrl: './asset.component.html',
	styleUrls: ['./asset.component.css']
})
export class AssetComponent implements OnInit, OnDestroy {
	contract!: string;
	tokenId!: number;
	asset!: any;
	voucher!: {};
	owner!: string;
	activitys: ItemActivity[] = []
	itemId = "0";
	url = false;
	tmp: any;
	public actuelUser = new User();
	address = "";
	public config = {
		apiKey: environment.API_KEY,
		network: Network.MATIC_MAINNET,
		maxRetries: 10
	};
	public alchemy = initializeAlchemy(this.config);
	order: any;
	hasOrder = false;
	orderSup!: Subscription;
	userrSup!: Subscription;
	loginUser: boolean = false;
	addressUserView: boolean = false

	constructor(private db: UserService,
		private spinnerService: NgxSpinnerService,
		private sanitizer: DomSanitizer,
		private router: ActivatedRoute,
		private Lazyservice: LazymintingService,
		private auth: AuthService,
		private cdr: ChangeDetectorRef,
		private orderService: OrderService,
		private contractsService: ContractsService,
	) { }

	ngOnDestroy(): void {
		this.orderSup.unsubscribe()
		if (this.userrSup)
			this.userrSup.unsubscribe()
	}

	async ngOnInit(): Promise<void> {
		this.auth.loginUser.subscribe((res: boolean) => {
			this.loginUser = res;
			(!this.loginUser) ? this.addressUserView = false : this.addressUserView = true;
			this.cdr.detectChanges();
		});
		this.auth.addressUser.subscribe((res: string) => {
			this.address = res;
			this.cdr.detectChanges();
		})
		this.url = false
		this.spinnerService.show()
		this.router.params.subscribe(async params => {
			this.contract = await params['contract'];
			this.tokenId = await params['tokenid'];
			await this.history('0x0')
			this.owner = await this.contractsService.getOwner(this.contract, this.tokenId.toString())
			this.getUser()
			this.owner = this.owner.toLowerCase()
			this.asset = await getNftMetadata(this.alchemy,
				this.contract,
				this.tokenId
			)
			console.log(this.asset)
			this.itemId = this.asset.rawMetadata.id.split(":")[5]
			this.url = true
			this.spinnerService.hide()
			this.retrieveOrders()
		})
	}
	async listingWearable() {
		const status = await this.auth.switchNetwork("0x89", "Polygon")
			if (status == true) {
		const { value: price } = await Swal.fire({
			title: 'List for sale',
			input: 'number',
			inputAttributes: {
				min: '0',
				accept: 'all',
			},
			inputValidator: (value) => {
				return new Promise((resolve) => {
					if (!value) {
						resolve('You must enter a number')
					} else {
						resolve('')
					}
				})
			},
			inputLabel: 'Set a price',
			inputPlaceholder: 'Amount',
			showCancelButton: true,
			allowOutsideClick: false,
			confirmButtonText: "Complete listing"
		})
		if (price) {
			let isApproved = false
			let isAdded = false
			Swal.fire({
				showConfirmButton: false,
				title: 'List for sale',
				html: "<ul class='text-start' style='list-style-type: none;'><li><span class='fw-bold fs-6 mb-1 Lato' style='color:brown;font-size: 20px;'>Approve :</span>&nbsp; You will be asked to approve this collection from your wallet. You only need to approve each collection once.</li> <br><li><span class='fw-bold fs-6 mb-1 Lato' style='color:brown;;font-size: 20px;'> Add offer :</span>&nbsp You will be asked to review and confirm this listing from your wallet.</label></li></ul>",
				allowOutsideClick: false,
				didOpen: () => {
					Swal.showLoading(Swal.getDenyButton())
				}
			})
			try {
				isApproved = await this.contractsService.Approve(this.contract, this.address, 1)
				if (isApproved) {
					isAdded = await this.Lazyservice.AddSignature(this.asset?.tokenUri?.raw, price, 1, 1, 0, this.tokenId, this.address, this.contract, this.asset?.rawMetadata?.image, this.asset?.title, this.asset.rawMetadata?.attributes[1]?.['value'])
					Swal.close()
				}
			} catch (error) {
				Swal.close()
			}
		}}
	}

	retrieveOrders(): void {
		this.orderSup = this.orderService.getOrderByitemId(this.contract, this.tokenId.toString()).subscribe(async result => {
			this.order = result;
			if (this.order.length > 0) {
				this.order[0].price = Number(this.order[0].price);
				this.hasOrder = true;
			} else {
				this.hasOrder = false;
			}
		})
	}

	async BuyWearable() {

		if (this.loginUser == false) {
			Swal.fire({
				imageUrl: '../../../assets/img/metamask.gif',
				imageWidth: 80,
				imageHeight: 70,
				title: "<div style='margin: 0 0 -5px !important;padding: 0 !important;'>You mast connect your wallet</div>",
				confirmButtonText: '<div style="width: 15em;">Connect wallet</div>',
				confirmButtonColor: '#3085d6',
				showCancelButton: true,
				cancelButtonText: "<div style='width: 15em;'>No thanks</div>",
				allowOutsideClick: false,
				width: 600,
			}).then(async (result) => {
				if (result.isConfirmed) {
					await this.auth.connect()
				}
			})
		} else {
			const status = await this.auth.switchNetwork("0x89", "Polygon")
			if (status == true) {
				Swal.fire({
					showConfirmButton: false,
					title: 'Confirm the transaction to purchase this item',
					allowOutsideClick: false,
					didOpen: () => {
						Swal.showLoading(Swal.getDenyButton())
					}
				})

				this.voucher = {
					contractAddress: this.contract,
					tokenId: this.tokenId.toString(),
					tokenOwner: this.order[0].OwnerAddress,
					price: this.order[0].price.toString(),
					amount: 1,
					amountForSale: 1,
					royalties: this.order[0].royalties,
					uri: this.order[0].metadata,
					nonce: this.order[0].nonce,
					signature: this.order[0].signature,
				}
				try {
					if (this.loginUser) {
						await this.Lazyservice.lazyMinting(this.address, this.order[0].id, this.voucher, 1);
						await this.ngOnInit()
						Swal.close()
					}
				} catch (error) {
					Swal.close()
				}
			}
		}
	}
	async CancelListing() {
		Swal.fire({
			title: "<div style='margin: 0 0 -5px !important;padding: 0 !important;'>Are you sure?</div>",
			icon: 'warning',
			showCancelButton: true,
			cancelButtonText: "No,don't cancel",
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes, cancel it!'
		}).then(async (result) => {
			if (result.isConfirmed) {
				await this.orderService.removeOrder(this.order[0].id);
				Swal.fire({
					showConfirmButton: false,
					title: "<div style='margin: 0 0 -5px !important;padding: 0 !important;'>Canceled!</div>",
					icon: 'success',
					timer: 2500
				}
				)
			}
		})

	}
	AanimationURL() {
		return this.sanitizer.bypassSecurityTrustResourceUrl("https://wearable-preview.decentraland.org/?contract=" + this.contract + "&item=" + this.itemId);
	}

	async getUser() {
		this.userrSup = this.db.getUser(this.owner.toLowerCase()).subscribe(async result => {
			this.tmp = result
			if (this.tmp != undefined) {
				this.actuelUser = new User(this.tmp?.banner_image, this.tmp?.profile_image, this.tmp?.email, this.tmp?.username, this.tmp?.address)
			}
		})
	}

	async history(from: string) {
		if (from == "0x0") {
			this.activitys = []
		}
		const options = {
			method: 'POST',
			headers: { Accept: 'application/json', 'Content-Type': 'application/json' },
			body: JSON.stringify({
				id: 0,
				jsonrpc: '2.0',
				method: 'alchemy_getAssetTransfers',
				params: [
					{
						fromBlock: from,
						contractAddresses: [this.contract],
						category: ['erc721'],
						withMetadata: true,
						excludeZeroValue: false,
						maxCount: '0x3e8',
					}
				]
			})
		};
		let i = 0
		fetch(environment.API_URL, options)
			.then(response => response.json())
			.then(async response => response.result.transfers.forEach(async (element: any) => {
				i++
				if (BigInt(element.erc721TokenId) === BigInt(this.tokenId)) {
					let date = Date.now() - new Date(element.metadata.blockTimestamp).getTime()
					let time = this.dhm(date)
					this.activitys.push(new ItemActivity(
						element.from === "0x0000000000000000000000000000000000000000" ? "Mint" : "Transfer",
						0,
						element.from,
						element.to,
						time,
						element.hash
					))
				}
				if (i == 1000) {
					this.history(element.blockNum)
				}
				if (response.result.transfers.length < 1000 && i == response.result.transfers.length - 1) {
					this.getprices()
				}
			}
			)
			).catch(err => console.error(err));
	}
	async getprices() {
		for await (const activity of this.activitys) {
			await fetch("https://api.polygonscan.com/api?module=proxy&action=eth_getTransactionByHash&txhash=" + activity.hash + "&apikey=RRSN4FCBKNAE6HVJWRPF4343NDII8JCDPG").then(async response => await response.json())
				.then(async response => activity.price = Number(await response.result.value) / 1000000000000000000)
		}
	}

	dhm(t: number) {
		var cd = 24 * 60 * 60 * 1000,
			ch = 60 * 60 * 1000,
			d = Math.floor(t / cd),
			h = Math.floor((t - d * cd) / ch),
			m = Math.round((t - d * cd - h * ch) / 60000),
			pad = function (n: number) { return n < 10 ? '0' + n : n; };
		if (m === 60) {
			h++;
			m = 0;
		}
		if (h === 24) {
			d++;
			h = 0;
		}
		return [d + " day", pad(h) + " hours"].join(' ');
	}


}



export class ItemActivity {
	event!: string;
	price!: number;
	from!: string;
	to!: string;
	date!: string;
	hash!: string;
	constructor(event: string, price: number, from: string, to: string, date: string, hash: string) {
		this.event = event,
			this.price = price,
			this.from = from,
			this.to = to,
			this.date = date,
			this.hash = hash
	}
}
