import { ChangeDetectorRef, Component, OnDestroy, OnInit } from '@angular/core';
import { User } from 'src/app/models/user'
import { UserService } from 'src/app/services/firebase-services/user.service';
import { Subscription } from 'rxjs';
import { AuthService } from 'src/app/services/auth.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})

export class NavbarComponent implements OnInit, OnDestroy {
  public ShowNavbar = true;
  balance: number | undefined;
  public user = new User();
  public unsub!: Subscription
  loginUser: boolean = false;
  addressUser: string = ''
  addressUserView: boolean = false
  constructor(private db: UserService, private auth: AuthService, private cdr: ChangeDetectorRef) {
  }

  ngOnDestroy(): void {
    this.unsub.unsubscribe()
  }

  async ngOnInit(): Promise<void> {

    this.auth.loginUser.subscribe((res: boolean) => {
      this.loginUser = res;
      (!this.loginUser) ? this.addressUserView = false : this.addressUserView = true;
      this.cdr.detectChanges();
    });
    this.auth.addressUser.subscribe((res: string) => {
      this.addressUser = res;
      this.getUser(this.addressUser)
      this.cdr.detectChanges();
    })

    //event for displat the navbar
    document.addEventListener('pointerlockchange', event => {
      if (document.pointerLockElement) {
        this.ShowNavbar = false;
      } else {
        this.ShowNavbar = true;
      }
    });

  }
  connect() {
    this.auth.connect()
  }
  logout() {
    this.auth.logout()
  }

  async getUser(account: string) {
    if (account !== undefined && account !== '') {
      this.unsub = this.db.getUser(account).subscribe(async result => {
        if (result != undefined) {
          this.user = result as User
        } else {
          this.db.login(account)
        }
      })
      //Get the user balance 
      this.balance = await this.auth.getBalance(account)
      this.cdr.detectChanges();
    }
  }
  NotAdded() {
    Swal.fire({ title: 'this feature is not added yet', icon: 'info', timer: 3000 })
  }
}
