import { ChangeDetectorRef, Component, OnDestroy, OnInit } from '@angular/core';
import { OrderService } from 'src/app/services/firebase-services/order.service';
import { User } from 'src/app/models/user';
import { environment } from 'src/environments/environment';
import { UserService } from 'src/app/services/firebase-services/user.service';
import { Werabeals } from 'src/app/models/werabeals';
import { RoomsService } from 'src/app/services/firebase-services/rooms.service';
import { ToastrService } from 'ngx-toastr';
import { IpfsService } from 'src/app/services/ipfs.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { ActivatedRoute, Event, NavigationEnd, Router } from '@angular/router';
import { AlchemyService } from 'src/app/services/alchemy.service';
import { map, Subscription } from 'rxjs';
import { Clipboard } from '@angular/cdk/clipboard';
import { Room } from 'src/app/models/room';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-myrooms',
  templateUrl: './myrooms.component.html',
  styleUrls: ['./myrooms.component.scss']
})
export class MyroomsComponent implements OnInit, OnDestroy {
  public user = new User();
  public OwnerAddress!: string;
  public myRooms: Room[] = [];
  items: any[] = [];
  filter: any[] = [];
  offers: any[] = [];
  part!: string;
  myTemplates: any[] = []
  profileImage!: any;
  bannerImage!: any;
  public Met3d !: string;
  banner: any
  profile: string | undefined
  public skeletonloader = true;
  public skeletonloaderDCW = true;
  public skeletonloaderO = true;
  public skeletonloaderS = true;
  public objectsMeta: Werabeals[] = [];

  tmp: any;
  routesup: any;
  supUserData!: Subscription;
  loginUser: boolean = false;
  addressUserView: boolean = false

  constructor(
    private clipboard: Clipboard,
    private orderService: OrderService,
    private db: UserService,
    private spinnerService: NgxSpinnerService,
    private dbservuce: RoomsService,
    private toastr: ToastrService,
    private ipfs: IpfsService,
    public router: Router,
    private route: ActivatedRoute,
    private alche: AlchemyService,
    private auth: AuthService, private cdr: ChangeDetectorRef
  ) {
    this.routesup = this.router.events.subscribe((event: Event) => {
      if (event instanceof NavigationEnd) {
        const sup2 = this.route.params.subscribe(async params => {
          this.part = await params['part']
          document.getElementById(this.part)?.click();
          sup2.unsubscribe()
        })
      }
      //sup.unsubscribe()
    });
    document.addEventListener("change address", async event => {
      await this.getData()
    })
  }
  ngOnDestroy(): void {
    this.routesup.unsubscribe()
    this.supUserData.unsubscribe()
  }
  async getOrderBySeller() {
    this.skeletonloaderO = true
    const sup = this.orderService.getOrderBySeller(this.OwnerAddress).subscribe(result => {
      this.offers = result;
      this.skeletonloaderO = false
      sup.unsubscribe()
    });
  }
  async ngOnInit() {
    this.auth.loginUser.subscribe((res: boolean) => {
      this.loginUser = res;
      (!this.loginUser) ? this.addressUserView = false : this.addressUserView = true;
      this.cdr.detectChanges();
    });
    this.auth.addressUser.subscribe(async (res: string) => {
      this.OwnerAddress = res;
      await this.getData()
      this.cdr.detectChanges();
    })
    

  }
  async loadMore() {

  }

  async getData() {
    if (!this.loginUser) {
      await this.auth.connect()
    }else{
      this.getUserData(this.OwnerAddress)
    await this.getOrderBySeller()
    await this.getDCwearables()
    this.dbservuce.ListOfMyRooms(this.OwnerAddress);
    this.getMyRooms();
    this.getMyTemplates()
    }
    
  }
  async getMyTemplates() {
    this.skeletonloader = true
    this.myTemplates = await this.alche.getNftsByContractAndUser(this.OwnerAddress, environment.TEMPLATE8CONTRACT);
    this.skeletonloader = false
  }

  async getDCwearables() {
    this.skeletonloaderDCW = true
    this.items = []
    this.filter = []
    this.items = await this.alche.getNftsForOwnerIterator(this.OwnerAddress)
    this.filter = this.items
    this.skeletonloaderDCW = false
  }

  Filter(sort: string) {
    this.skeletonloaderDCW = true
    document.getElementById("pills-home-tab")?.click();
    this.filter = []
    let j = 0
    if (sort != 'all') {
      for (let i = 0; i < this.items.length; i++) {
        if (this.items[i].rawMetadata?.attributes[1]?.['value'] == sort) {
          this.filter[j] = this.items[i]
          j++
        }
      }
    } else {
      this.filter = this.items
    }
    this.skeletonloaderDCW = false
  }

  public async uploadImage(eventTarget: any) {
    if (eventTarget.files) {
      var reader = new FileReader()
      this.profile = eventTarget.files[0]
      reader.readAsDataURL(eventTarget.files[0])
      reader.onload = (event: any) => {
        this.user.profile_image = event.target.result
      }
    }
  }

  public async uploadImage2(eventTarget: any) {
    if (eventTarget.files) {
      var reader = new FileReader()
      this.banner = eventTarget.files[0]
      reader.readAsDataURL(eventTarget.files[0])
      reader.onload = (event: any) => {
        this.user.banner_image = event.target.result
      }
    }
  }
  async update(username: string, email: string) {
    this.spinnerService.show()
    if (this.banner != undefined) {
      const cid = await this.ipfs.storPicture(this.banner)
      this.user.banner_image = "https://nftstorage.link/ipfs/" + cid
    }
    if (this.profile != undefined) {
      const cid = await this.ipfs.storPicture(this.profile)
      this.user.profile_image = "https://nftstorage.link/ipfs/" + cid
    }
    this.db.updateUser(this.OwnerAddress.toLowerCase(), username, email, this.user?.profile_image, this.user.banner_image);
    this.spinnerService.hide()
    this.toastr.success('profile has been updated', 'Success', {
      timeOut: 2000,
      positionClass: "toast-bottom-right",
    });
  }
  getUserData(address: string) {
    this.supUserData = this.db.getDataUser(address).snapshotChanges().pipe(
      map(changes =>
        ({ id: changes[0].payload.doc.id, ...changes[0].payload.doc.data() })
      )
    ).subscribe(data => {
      this.user = data;
      this.profileImage = this.user.profile_image;
      this.bannerImage = this.user.banner_image;
    });
  }

  getMyRooms() {
    this.skeletonloaderS = true
    const sup = this.dbservuce.ListOfMyRooms(this.OwnerAddress).subscribe(data => {
      this.skeletonloaderS = false
      this.myRooms = data.reverse() as unknown as Room[];
      sup.unsubscribe()
    });
  }
  deleteRoom(id: string, chatId: string,eventsId:string) {
    if (window.confirm("Are you sure that you wanted to delete that Room?")) {
      this.dbservuce.delete(id, chatId,eventsId);
      this.getMyRooms();
    }
  }
  copyText() {
    this.clipboard.copy(this.OwnerAddress);
    this.showSuccess("Copied!")
  }
  showSuccess(message: string) {
    this.toastr.success(message, '', {
      timeOut: 500,
      positionClass: "toast-bottom-right",
    });
  }

}
