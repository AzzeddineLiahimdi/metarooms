import { ChangeDetectorRef, Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { OrderService } from 'src/app/services/firebase-services/order.service';
import { LazymintingService } from 'src/app/services/lazyminting.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { User } from 'src/app/models/user';
import { UserService } from 'src/app/services/firebase-services/user.service';
import { ContractsService } from 'src/app/services/contracts.service';
import { AlchemyService } from 'src/app/services/alchemy.service';
import { Subscription } from 'rxjs';
import { AuthService } from 'src/app/services/auth.service';
import Swal from 'sweetalert2';

@Component({
	selector: 'app-assets',
	templateUrl: './assets.component.html',
	styleUrls: ['./assets.component.css']
})
export class AssetsComponent implements OnInit, OnDestroy {
	orderSub!: Subscription;
	contract!: string;
	tokenId!: number;
	asset!: any;
	owners!: any;
	activitys: any[] = []
	url = false;
	address = "";
	tmp: any;
	balance = 0;
	orders: any[] = []
	dataorders: any[] = []
	voucher!: {};
	balanceforsell!: number;
	public Conected = false;
	public actuelUser = new User();
	loginUser: boolean = false;
	addressUserView: boolean = false

	constructor(private db: UserService,
		private spinnerService: NgxSpinnerService,
		private router: ActivatedRoute,
		private Lazyservice: LazymintingService,
		private orderService: OrderService,
		private contractsService: ContractsService,
		private achemy: AlchemyService,
		private auth: AuthService,
		private cdr: ChangeDetectorRef,
	) { }
	ngOnDestroy(): void {
		this.orderSub.unsubscribe()
	}

	async ngOnInit() {
		this.auth.loginUser.subscribe((res: boolean) => {
			this.loginUser = res;
			(!this.loginUser) ? this.addressUserView = false : this.addressUserView = true;
			this.cdr.detectChanges();
		});
		this.auth.addressUser.subscribe((res: string) => {
			this.address = res;
			this.cdr.detectChanges();
		})
		this.url = false
		this.spinnerService.show()
		this.router.params.subscribe(async params => {
			this.contract = await params['contract'];
			this.tokenId = await params['tokenid'];

			this.asset = await this.achemy.getMetadata(this.contract, this.tokenId)
			console.log(this.asset)
			this.owners = await this.achemy.getOwnersForNft(this.asset)
			this.url = true
			await this.getaddressBalance(this.address)
			this.retrieveOrders()
			this.spinnerService.hide()
		})

	}

	async getaddressBalance(address: string) {
		let balance = await this.contractsService.getaddressBalance(address, this.tokenId)
		this.balance = Number(balance)
		this.balanceforsell = this.balance
	}

	getUser(owner: string) {
		return new Promise((resolve, reject) => {
			this.db.getUser(owner.toLowerCase()).subscribe(data => {
				this.tmp = data
				if (this.tmp != undefined) {
					resolve(new User(this.tmp?.banner_image, this.tmp?.profile_image, this.tmp?.email, this.tmp?.username, this.tmp?.wallet_address))
				} else {
					resolve(new User())
				}
			});
		})
	}

	async SellDialog() {
		const status = await this.auth.switchNetwork("0x89", "Polygon")
		if (status == true) {
		const { value: formValues } = await Swal.fire({
			title: 'List item for sale',
			html:
				'<div class="mb-3"><label for="fname">Amount</label> <input id="amount-input" style="width:80%;margin: 0.5em 2em 3px;" type="number" id="input1" class="swal2-input"></div>' +
				'<div class="mb-3"><label for="fname">Price</label><input style="width:80%;margin: 0.5em 2em 3px;" id="price-input" type="number" class="swal2-input"></div>',
			focusConfirm: false,
			confirmButtonText: "Sell",
			showCancelButton: true,
			preConfirm: () => {
				const amount = document.getElementById('amount-input') as any
				const price = document.getElementById('price-input') as any
				return [
					amount.value, price.value
				]
			}
		})
		if (formValues) {
			const amount = formValues[0]
			const price = formValues[1]
			console.log(price, amount)

			if (Number(price) > 0 && Number(amount) > 0) {
				let isApproved = false
				let isAdded = false
				Swal.fire({
					showConfirmButton: false,
					title: 'List for sale',
					html: "<ul class='text-start' style='list-style-type: none;'><li><span class='fw-bold fs-6 mb-1 Lato' style='color:brown;font-size: 20px;'>Approve :</span>&nbsp; You will be asked to approve this collection from your wallet. You only need to approve each collection once.</li> <br><li><span class='fw-bold fs-6 mb-1 Lato' style='color:brown;;font-size: 20px;'> Add offer :</span>&nbsp You will be asked to review and confirm this listing from your wallet.</label></li></ul>",
					allowOutsideClick: false,
					didOpen: () => {
						Swal.showLoading(Swal.getDenyButton())
					}
				})
				try {
					isApproved = await this.contractsService.Approve(this.contract, this.address, 2)
					if (isApproved) {
						isAdded = await this.Lazyservice.AddSignature(this.asset?.tokenUri?.raw, Number(price), Number(amount), 1, 0, this.tokenId, this.address, this.contract, this.asset?.rawMetadata?.image, this.asset?.title, "Template")
						Swal.close()
					}
				} catch (error) {
					Swal.close()
				}
			}
		}}
	}

	retrieveOrders(): void {
		this.orderSub = this.orderService.getOrderByitemId(this.contract, this.tokenId.toString()).subscribe(async result => {
			this.orders = result;
			this.dataorders = []
			for (let i = 0; i < this.orders.length; i++) {
				this.orders[i].price = Number(this.orders[i].price)
				this.dataorders[i] = {
					order: this.orders[i],
					seller: await this.getUser(this.orders[i].OwnerAddress)
				}
				if (this.orders[i].OwnerAddress == this.address) {
					this.balanceforsell = this.balanceforsell - this.orders[i].amounttosall
				}
			}
			console.log(this.dataorders)
		})
	}
	async CancelListing(id: string) {
		if (window.confirm("Are you sure that you wanted to delete that Room?")) {
			try {
				await this.orderService.removeOrder(id);
			} catch (error) {
				console.log("Undo operation")
			}
		}
	}
	async BuyWearable(order: any) {
		if (this.loginUser == false) {
			Swal.fire({
				imageUrl: '../../../assets/img/metamask.gif',
				imageWidth: 80,
				imageHeight: 70,
				title: "<div style='margin: 0 0 -5px !important;padding: 0 !important;'>You mast connect your wallet</div>",
				confirmButtonText: '<div style="width: 15em;">Connect wallet</div>',
				confirmButtonColor: '#3085d6',
				showCancelButton: true,
				cancelButtonText: "<div style='width: 15em;'>No thanks</div>",
				allowOutsideClick: false,
				width: 600,
			}).then(async (result) => {
				if (result.isConfirmed) {
					await this.auth.connect()
				}
			})
		} else {
			const status = await this.auth.switchNetwork("0x89", "Polygon")
			if (status == true) {
				Swal.fire({
					showConfirmButton: false,
					title: 'Confirm the transaction to purchase this item',
					allowOutsideClick: false,
					didOpen: () => {
						Swal.showLoading(Swal.getDenyButton())
					}
				})

				this.voucher = {
					contractAddress: this.contract,
					tokenId: this.tokenId.toString(),
					tokenOwner: order.OwnerAddress,
					price: order.price.toString(),
					amount: 100,
					amountForSale: order.amounttosall,
					royalties: order.royalties,
					uri: order.metadata,
					nonce: order.nonce,
					signature: order.signature,
				}
				try {
					if (this.loginUser) {
						await this.Lazyservice.lazyMinting(this.address, order.id, this.voucher, 2);
						Swal.close()
						await this.ngOnInit()
					}
				} catch (error) {
					Swal.close()
					alert("buy canceled")
				}
			}
		}
	}

}

