import { Component, OnDestroy, OnInit } from '@angular/core';
import { PaginationService } from 'src/app/services/firebase-services/pagination.service';

@Component({
  selector: 'app-market',
  templateUrl: './market.component.html',
  styleUrls: ['./market.component.css']
})
export class MarketComponent implements OnInit,OnDestroy {
  public type!:string| null
  public category!: string | null
  public rangeMin!:number | null
  public rangeMax!:number | null
  public rangeError=false
  
  constructor(
    public page: PaginationService,
  ) { this.type=null,this.category=null,this.rangeMax=null,this.rangeMin=null}

  ngOnDestroy(): void {
    this.page.sub.unsubscribe()
  }
  async ngOnInit() {
    this.page.init('listings', 'createdAt', { reverse: true,limit:12})
  }
  
  filterByType(type: string | null) {
    this.page.sub.unsubscribe()
    this.type = type
    this.category =null
    this.page.init('listings', 'createdAt', {limit:12,rangeValue1:this.rangeMin,rangeValue2:this.rangeMax,attribut1:"type",value1:this.type,attribut2:"Category",value2:this.category})
  }

  filterByCategory(cat: string | null) {
    this.page.sub.unsubscribe()
    this.category = cat
    this.page.init('listings', 'createdAt', {limit:12,rangeValue1:this.rangeMin,rangeValue2:this.rangeMax,attribut1:"type",value1:this.type,attribut2:"Category",value2:this.category })
  }

  alert() {
    alert("this status filter is not yet integrated")
  }

  FilterPrice(minprice:string,maxprice:string) {
    this.rangeError=Number(minprice)>Number(maxprice) && Number(maxprice)!=0
    if(!this.rangeError ){
    this.rangeMin=Number(minprice)
    this.rangeMax=Number(maxprice)
    this.page.init('listings', 'createdAt', {limit:12,rangeValue1:this.rangeMin,rangeValue2:this.rangeMax,attribut1:"type",value1:this.type,attribut2:"Category",value2:this.category })
    }  
  }
  LoadMore() {
    this.page.more()
  }
  
}
