import { SpinnerService } from '../../services/spinner.service';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { RoomsService } from 'src/app/services/firebase-services/rooms.service';
import { ActivatedRoute, Router } from "@angular/router"
import { Component, OnInit, Input, HostListener, Renderer2, OnDestroy} from '@angular/core';
import { UnityService } from 'src/app/services/unity.service';
import { Room } from 'src/app/models/room';
import { AuthService } from 'src/app/services/auth.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-spinner',
  templateUrl: './spinner.component.html',
  styleUrls: ['./spinner.component.scss']
})

export class SpinnerComponent implements OnInit,OnDestroy {
  @Input() progress: number | undefined;
  @Input() message: string | undefined;
  @HostListener('document:keydown.meta.k')
  showSpinner = false;
  showButton = false;
  Showtoggle = true;
  isCheckedMode = false;
  public loding = false;
  public Description: string | undefined;
  public title: string | undefined;
  public showSelect: boolean = true;
  public mobile=false
  selected = "Decentraland";
  avatarUrl = "";
  username = "";
  public uploadForm = new FormGroup({
    username: new FormControl(''),
    avatarurl: new FormControl(''),
  })
  subscribeRoom!: Subscription;
  spinnerSubscribe: any;
  subUserData!: Subscription;

  constructor(
    private spinnerService: SpinnerService,
    private router: Router,
    private renderer: Renderer2,
    private route: ActivatedRoute,
    public roomservice: RoomsService,
    public unityService: UnityService,
    private auth: AuthService,
  ) {
    if (/iPhone|iPad|iPod|Android/i.test(navigator.userAgent)) {
      this.mobile=true
    }
    this.loding = false;
    this.showButton = true;
    this.progress = 0;
    if (router.url.includes("editmode")) {
      this.unityService.mode = '0'
    } else {
      this.unityService.mode = '1'
    }
    const subscribeRoute=this.route.params.subscribe(async params => {
      this.unityService.sceneName = await params['seneName']
      const roomId = await params['RoomId']
      this.subscribeRoom=roomservice.GetRoomById(roomId).subscribe(result => {
        this.unityService.room = result as Room
        this.title = this.unityService.room.roomName
        this.subscribeRoom.unsubscribe()
      })
      this.Description = "Teleport your DCL Avatar by safely connecting your wallet. Experience " +this.unityService.sceneName +" with others by entering in multiplayer mode.";
    })
    subscribeRoute.unsubscribe()
    

    this.renderer.listen('document', 'keypress', e => {
      if (e.keyCode == 10 && this.showSelect == true) {
        this.unityService.InputCapture("false")
        this.showSelect = false;
      } else if (e.keyCode == 10 && this.showSelect == false) {
        this.unityService.InputCapture("true")
        this.showSelect = true;
        this.selected = "Decentraland";
      }
    });
  }

  ngOnDestroy() {
    this.progress = 0;
    this.spinnerService.resetSpinner();
    this.subscribeRoom.unsubscribe()
    this.spinnerSubscribe.unsubscribe()
    if(this.subUserData){
      this.subUserData.unsubscribe()
    }
  }

  ngOnInit() {
    this.init();
    document.addEventListener("enter_ready", () => {
      this.showButton = false;
    });
  }

  init() {
   this.spinnerSubscribe= this.spinnerService.getSpinnerObserver().subscribe((status) => {
      this.showButton = (status === 'start');
      if (this.router.url.includes("/space")) this.showSpinner = true;
      if (status === 'stop') this.showSpinner = false;
    });
  }

  onSubmit(mode: number) {
    this.showButton = true;
    this.loding = true;
    this.Showtoggle = false;
    if (this.uploadForm.valid && mode == 1) this.ConnectWithWallet();
    if (this.uploadForm.valid && mode == 2 ) this.connectAsGuest();
  }

  async connectAsGuest() {
    await this.unityService.SendAvatarData(this.selected, this.selected, this.avatarUrl)
    await this.unityService.loadGame(this.isCheckedMode)
  }
  async ConnectWithWallet(){
    const metamaskInstalled = this.auth.isMetaMaskInstalled()
      this.subUserData=this.auth.loginUser.subscribe(async (res: boolean) => {
        if (res) {
          await this.unityService.SendWalletData()
          await this.unityService.SendAvatarData(this.selected, this.selected, this.avatarUrl)
          await this.unityService.loadGame(this.isCheckedMode)
        }else{
          this.auth.connect()
        }
      })

    if (!metamaskInstalled) {
      this.connectAsGuest()
    }
  }


  validator(selected: any) {
    if (selected.value === "Ape") {
      this.uploadForm = new FormGroup({
        username: new FormControl('', Validators.required),
      })
    }
    if (selected.value === "Ready Player Me") {
      const urlRegex = /^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$/;
      this.uploadForm = new FormGroup({
        username: new FormControl('', Validators.required),
        avatarurl: new FormControl("", [Validators.required, Validators.pattern(urlRegex)]),
      })
    }
  }

}