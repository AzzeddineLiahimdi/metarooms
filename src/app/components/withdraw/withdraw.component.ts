import { Component, OnInit } from '@angular/core';
import { ContractsService } from 'src/app/services/contracts.service';

@Component({
  selector: 'app-withdraw',
  templateUrl: './withdraw.component.html',
  styleUrls: ['./withdraw.component.css']
})
export class WithdrawComponent implements OnInit {
  value = '';
  Balance!: any;
  constructor( private marketService: ContractsService
    ) {
    
   }

  async ngOnInit(): Promise<void> {
    await this.getBalance();
  }
  async getBalance() {
    this.Balance = await this.marketService.getContractBalance();
    console.log(this.Balance,"my balance")
  }
  async withdraw() {
    this.Balance = await this.marketService.withdraw(this.value);
    this.getBalance()
  }
  

}
