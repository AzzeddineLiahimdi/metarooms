import { Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { ChatService } from 'src/app/services/chat.service';
import { UserService } from 'src/app/services/firebase-services/user.service';
import { Observable, Subscription } from 'rxjs';
import { User } from 'src/app/models/user';
import { UnityService } from 'src/app/services/unity.service';
import { ActivatedRoute } from '@angular/router';
import { RoomsService } from 'src/app/services/firebase-services/rooms.service';
import { Room } from 'src/app/models/room';
import { AuthService } from 'src/app/services/auth.service';
declare function chatboxOpen(): any;
declare function chatboxClose(): any;
declare function chatboxMaximize(): any;
declare function chatboxMinimize(): any;
declare function chatboxPanelClose(): any;

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.css']
})
export class ChatComponent implements OnInit,OnDestroy {
  chat$!: Observable<any>;
  newMsg!: string;
  account!: User;
  message=0;
  count!:number
  sub!: Subscription;
  @ViewChild('scrollMe') private myScrollContainer!: ElementRef;
  @ViewChild('scrollMe2') private myScrollContainer2!: ElementRef;

  public roomId?: string;
  subscription!: Subscription;
  constructor(
    public cs: ChatService,
    public auth: UserService,
    private route: ActivatedRoute,
    public unityservice: UnityService,
    public roomservice: RoomsService,
    private login: AuthService
  ) {
    document.addEventListener("change address", async event => {
      await this.ngOnInit()
    })
  }
  ngOnDestroy(): void {
    if(this.subscription)
    this.subscription.unsubscribe()
    if(this.sub)
    this.sub.unsubscribe()
  }

  async ngOnInit() {
    const addressUser = await this.login.addressUser.getValue()
    if (addressUser!= "") {
      const s1 = this.auth.getUser(addressUser).subscribe(result => {
        this.account = result as User
        s1.unsubscribe()
      });
   
    const roomId = this.route.snapshot.paramMap.get('RoomId');
    if(roomId)
    this.subscription = this.roomservice.GetRoomById(roomId.toString()).subscribe(result => {
      const ROOM = result as Room
      if (ROOM.chatId) {
        const source = this.cs.get(ROOM.chatId);
        this.chat$ = this.cs.joinUsers(source);
        this.sub=this.chat$.subscribe(result=>{
          if(result.messages.length>this.count && this.count!=0 && result.messages[result.messages.length-1].uid!=addressUser){
            this.count=result.messages.length
            this.message++
          }else{
            this.count=result.messages.length
          }
        })
      }
      this.subscription.unsubscribe()
    });
  }
  }

  async submit(chatId: string) {
    if (this.account.wallet_address && this.newMsg!="")
    await this.cs.sendMessage(chatId, this.newMsg, this.account.wallet_address);
    this.scrollToBottom(); 
    this.newMsg = '';
  }
  trackByCreated(i: any, msg: any) {
    return msg.createdAt;
  }
  
  chatOpen(){
    try {
      this.unityservice.InputCapture('false')
      chatboxOpen()
      this.scrollToBottom(); 
      this.message=0
    } catch (error) {
      console.log(error)
    }


  }
  chatClose(){
    try {
      this.unityservice.InputCapture('true')
      chatboxClose()
      this.message=0
    } catch (error) {
      console.log(error)
    } 
  }
  chatMaximize(){
    chatboxMaximize()
    this.scrollToBottom(); 
  }
  chatMinimize(){
    chatboxMinimize()
  }
 chatPanelClose(){
  try {
    this.unityservice.InputCapture('true')
    chatboxPanelClose()
  } catch (error) {
    console.log(error)
  }
  }


scrollToBottom(): void {
        this.myScrollContainer.nativeElement.scrollTop = this.myScrollContainer.nativeElement.scrollHeight;
        this.myScrollContainer2.nativeElement.scrollTop = this.myScrollContainer2.nativeElement.scrollHeight;
}
}