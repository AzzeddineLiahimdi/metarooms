import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Observable, Subscription } from 'rxjs';
import { Room } from 'src/app/models/room';
import { User } from 'src/app/models/user';
import { EventModule } from 'src/app/models/utilities/event/event.module';
import { AlchemyService } from 'src/app/services/alchemy.service';
import { RoomEventsService } from 'src/app/services/firebase-services/room-events.service';
import { RoomsService } from 'src/app/services/firebase-services/rooms.service';
import { UserService } from 'src/app/services/firebase-services/user.service';

@Component({
  selector: 'app-event',
  templateUrl: './event.component.html',
  styleUrls: ['./event.component.css']
})
export class EventComponent implements OnInit, OnDestroy {
  event$!: Observable<EventModule>;
  account!: User;
  action = false;
  sub!: Subscription;
  sub2!: Subscription;
  last = 0


  constructor(
    private toastr: ToastrService,
    private es: RoomEventsService,
    private route: ActivatedRoute,
    public roomservice: RoomsService,
    public auth: UserService,
    public alche: AlchemyService
  ) { }

  ngOnDestroy(): void {
    this.sub.unsubscribe()
    if(this.sub2){this.sub2.unsubscribe()}
    
  }

  async ngOnInit(): Promise<void> {
    const roomId = this.route.snapshot.paramMap.get('RoomId');
    if (roomId)
      this.sub = this.roomservice.GetRoomById(roomId).subscribe(result => {
        const ROOM = result as Room
        if (ROOM.eventsId) {
          this.event$ = this.es.get(ROOM.eventsId);
          this.sub2 = this.event$.subscribe(result => {
            if (result.eventMessage.length > this.last) {
              this.last = result.eventMessage.length
              this.showSuccess(result.eventMessage[result.eventMessage.length - 1]?.content)
            } else {
              this.last = result.eventMessage.length
            }
          })
        }
        this.sub.unsubscribe()
      });
  }

  showSuccess(message: string) {
    this.toastr.info(message, "", {
      timeOut: 30000,
      progressBar: true,
      progressAnimation: 'decreasing'
    });
  }
}
