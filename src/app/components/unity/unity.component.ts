import { Component, OnInit, OnDestroy } from '@angular/core';
import { SpinnerService } from '../../services/spinner.service';
import { UnityService } from 'src/app/services/unity.service';
import { ToastrService } from 'ngx-toastr';


@Component({
  selector: 'app-unity',
  templateUrl: './unity.component.html',
  styleUrls: ['./unity.component.css']
})
export class UnityComponent implements OnInit, OnDestroy {

  public inpage = true;
  public progress = 0;
  public message = "Creating instance ..."
  public videourl!:string
  public mobile=false

  constructor(
    private spinnerService: SpinnerService,
    public  unityService:UnityService,
    private toastr: ToastrService,
  ) {
    
  }

  async Unlockponter() {
    if (document.pointerLockElement && this.unityService.gameStarted) {
      //await this.unityService.InputCapture("true")
    } else if (this.unityService.gameStarted) {
      try {
       // await this.unityService.InputCapture("false")
        this.unityService.Unlockponter()
      } catch (error) {
      }
    }
  }

  ngOnDestroy(): void {
    let startEvent = new CustomEvent("quitinstance");
    document.dispatchEvent(startEvent);
    if (this.unityService.gameInstance === undefined) {
      this.inpage = false;
    } else {
      this.progress = 0;
      this.unityService.gameInstance.SendMessage('ReactConnector', 'ExitGame');
      this.unityService.gameInstance.Quit();
      this.unityService.gameInstance = undefined;
      document.cloneNode(true);
      console.log("Destroy Unity instance")
    }
  }
  async getPOAP(){
    window.open(this.unityService.link);
    document.getElementById("closeModal6")?.click();
    await this.unityService.PoapGeted()
  }
  async copyText() {
    navigator.clipboard.writeText(this.unityService.link);
    this.showSuccess("Copied!")
    document.getElementById("closeModal6")?.click();
    await this.unityService.PoapGeted()
  }
  showSuccess(message: string) {
    this.toastr.success(message, '', {
      timeOut: 2000,
      positionClass: "toast-bottom-right",
    });
  }

  async onSubmit(form: any) {
    console.log("onSubmit")
  if(form.value.url.length>0){
    document.getElementById("closeModal4")?.click();
    await this.unityService.sendVideoLink(form.value.url)
    this.videourl=""
  }else{
    this.unityService.InputCapture('true')
  }
}


  async ngOnInit() {
    this.spinnerService.requestStarted();
    var buildUrl = "/assets/WebglBuild/Build";
    var config = {
      dataUrl: buildUrl + "/WebglBuild.data",
      frameworkUrl: buildUrl + "/WebglBuild.framework.js",
      codeUrl: buildUrl + "/WebglBuild.wasm",
      streamingAssetsUrl: "/assets/WebglBuild/StreamingAssets",
      companyName: "Dinomite",
      productName: "City",
      productVersion: "0.1",
      devicePixelRatio: 0
    };

    let container = document.querySelector("#unity-container") || new Element();
    let canvas: HTMLElement = document.querySelector("#unity-canvas") || new HTMLElement();
    var loadingBar: HTMLElement = document.querySelector("#unity-loading-bar") || new HTMLElement();
    var progressBarFull: HTMLElement = document.querySelector("#unity-progress-bar-full") || new HTMLElement();
    var mobileWarning: HTMLElement = document.querySelector("#unity-mobile-warning") || new HTMLElement();

    if (/iPhone|iPad|iPod|Android/i.test(navigator.userAgent)) {
      this.mobile=true
      container.className = "unity-mobile";
      config.devicePixelRatio = 1;
      mobileWarning.style.display = "block";
      setTimeout(() => {
        mobileWarning.style.display = "none";
      }, 5000);
    } else {
      canvas.style.width = "100%";
      canvas.style.height = "100%";
    }
    loadingBar.style.display = "block";

    createUnityInstance(canvas, config, (progress: any) => {
      this.message = "Loading instance data ...";
      progressBarFull.style.width = 100 * progress + "%";
      this.progress = 100 * progress;
      if (this.progress < 70) {
        this.message = "Loading instance data ...";
      } else {
        this.message = "Loading avatar data ...";
      }
    }).then((unityInstance: any) => {
      this.message = "Create instance"
      if (this.inpage) {
        this.unityService.gameInstance = unityInstance;
        this.unityService.canvas=canvas
        loadingBar.style.display = "none";
        //prancipale event
        (window as any).OnUnityEvent = (jsonData: string) => {
          const data1=JSON.parse(jsonData)
          const data2=JSON.parse(data1.parameters)
          console.log(data1,data2,"jsonData")
          this.unityService.Treatment(jsonData)
        }
        document.addEventListener("quitinstance", () => {
          unityInstance.Quit();
        });
        document.addEventListener("pointerlockchange", () => {
          this.Unlockponter();
          
        });
        document.addEventListener("enter_ready", () => {
          this.message = "Open your instance"
        });
      } else {
        unityInstance.Quit();
      }
    }).catch((message: any) => {
      alert(message);
    });
  }
}
