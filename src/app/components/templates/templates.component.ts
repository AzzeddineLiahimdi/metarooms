import { ChangeDetectorRef, Component, OnDestroy, OnInit } from '@angular/core';
import { Template } from 'src/app/models/templates';
import { Router } from "@angular/router"
import { Room } from 'src/app/models/room';
import { TemplateService } from 'src/app/services/firebase-services/template.service';
import { RoomsService } from 'src/app/services/firebase-services/rooms.service';
import { ContractsService } from 'src/app/services/contracts.service';
import { ToastrService } from 'ngx-toastr';
import { ChatService } from 'src/app/services/chat.service';
import { RoomEventsService } from 'src/app/services/firebase-services/room-events.service';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-templates',
  templateUrl: './templates.component.html',
  styleUrls: ['./templates.component.css']
})
export class TemplatesComponent implements OnInit, OnDestroy {
  public address!: any;
  public room!: Room;
  template: Template = new Template();
  ListTemplates: any[] = []
  balance = 1
  roomName!: string
  roomDescription!: string
  public skeletonloader = true;
  private data!: any[]
  loginUser: boolean = false;
  addressUser: string = ''

  constructor(private dbservuce: RoomsService,
    private router: Router,
    private contract: ContractsService,
    private spacesService: TemplateService,
    private toastr: ToastrService,
    private chatservice: ChatService,
    private es: RoomEventsService,
    private auth: AuthService,
    private cdr: ChangeDetectorRef
  ) { }
  ngOnDestroy(): void {
    document.getElementById("closeModalButton2")?.click();
  }
  showSuccess(message: string) {
    this.toastr.success(message, 'Success', {
      timeOut: 6000,
    });
  }
  showError(message: string) {
    this.toastr.error(message, 'Error', {
      timeOut: 6000
    });
  }

  async ngOnInit(): Promise<void> {
    this.ListTemplates = []
    this.skeletonloader = true;
    this.auth.loginUser.subscribe((res: boolean) => {
      this.loginUser = res;
      console.log(this.loginUser)
      if (!this.loginUser) {
        document.getElementById("OpenModalButton")?.click();
      } else {
        document.getElementById("closeModalButton2")?.click();
      }
      this.cdr.detectChanges();
    });
    this.auth.addressUser.subscribe((res: string) => {
      this.addressUser = res;
      this.getAllTemplates()
      this.cdr.detectChanges();
    })
  }
  getAllTemplates() {
    this.ListTemplates = []
    this.skeletonloader = true
    if (!this.data) {
      this.spacesService.getAllTemplates().subscribe(result => {
        this.data = result
        this.skeletonloader = false
        this.ListTemplates = this.data
      });
    } else {
      this.skeletonloader = false
      this.ListTemplates = this.data
    }
  }

  getTemplateByCategory(category: string) {
    this.ListTemplates = []
    this.skeletonloader = true
    this.spacesService.getSpacesByCategory(category).subscribe(result => {
      this.skeletonloader = false
      this.ListTemplates = result
    })
  }

  async getBalanceOfToken(tokenId?: number) {
    let balance = 0
    balance = await this.contract.getaddressBalance(this.addressUser, tokenId)
    return balance
  }

  async changer(temp: Template) {
    let balance
    if (temp.collectable) {
      balance = await this.getBalanceOfToken(temp.tokenId)
      this.balance = Number(balance)
    }
    this.template = temp
    document.getElementById("OpenModalButton2")?.click();
  }

  async createRoom() {
    if (this.roomName != undefined && this.roomDescription != undefined) {
      if (this.addressUser != "") {
        const chatID = await this.chatservice.create(this.addressUser)
        const eventId = await this.es.create(this.addressUser)
        this.room = new Room(this.addressUser, this.roomName, this.roomDescription, this.template, chatID, eventId)
        this.dbservuce.createRoom(this.room);
        document.getElementById("closeModalButton")?.click();
        this.showSuccess("Room created")
        this.router.navigate(['/profile/pills-rooms-tab'])
      } else {
        this.showError("Connect your wallet")
      }
    } else {
      this.showError("You must fill in all the information to create your space")
    }
  }
  async ConnectWallet() {
    this.auth.connect()
  }
  LoadMore() {
    this.skeletonloader = true
    setTimeout(() => {
      this.skeletonloader = false
    }, 500);
  }
  goTo(temp: Template) {
    if (temp.collectable) {
      this.router.navigate(['./assets', temp.contractAddress, temp.tokenId]);
    }
  }


}
