import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TestComponent } from "./components/test/test.component";
import { HomePageComponent } from "./components/home-page/home-page.component";
import { TemplatesComponent } from "./components/templates/templates.component";
import { MyroomsComponent } from "./components/myrooms/myrooms.component";
import { NavbarComponent } from './components/navbar/navbar.component';
import {WithdrawComponent} from './components/withdraw/withdraw.component';
import { MarketComponent } from './components/market/market.component';
import { AssetComponent } from './components/asset/asset.component';
import { CreateTemplateComponent } from './components/create-template/create-template.component';
import { AssetsComponent } from './components/assets/assets.component';
import { UnityComponent } from './components/unity/unity.component';
import { PagenotfoundComponent } from './components/pagenotfound/pagenotfound.component';
import { HomeComponent } from './decentraland/home/home.component';
import { CollectionComponent } from './decentraland/collection/collection.component';
import { ItemComponent } from './decentraland/item/item.component';
import { ItemsComponent } from './decentraland/items/items.component';
const routes: Routes = [
  { 'path': '', redirectTo: 'homepage', pathMatch: 'full' },
  { 'path': 'test', component: TestComponent },
  { 'path': 'homepage', component: HomePageComponent },
  { 'path': 'myrooms', component: MyroomsComponent },
  { 'path': 'profile/:part', component: MyroomsComponent },
  { 'path': 'templates', component: TemplatesComponent },
  { 'path': 'navbar', component: NavbarComponent },
  { 'path': 'space/:seneName/:RoomId', component: UnityComponent},
  { 'path': 'space/editmode/:seneName/:RoomId', component: UnityComponent},
  { 'path': 'market',component:MarketComponent},  
  { 'path': 'withdraw', component: WithdrawComponent },
  { 'path':'asset/:contract/:tokenid',component:AssetComponent},
  { 'path':'assets/:contract/:tokenid',component:AssetsComponent},
  { 'path': 'create_space', component: CreateTemplateComponent },
  { 'path': 'decl/home', component: HomeComponent },
  {'path':'decl/collection/:collection',component:CollectionComponent},
  {'path':'adidas/collection/:collection',component:CollectionComponent},
  {'path':'decl/item/:collection/:itemId',component:ItemComponent},
  {'path':'decl/browse',component:CollectionComponent},
  {'path':'decl/collection/:network/:collection',component:CollectionComponent},
  {'path':'decl/asset/:network/:collection/:tokenId',component:ItemsComponent},
     //Wild Card Route for 404 request
     { path: '**', pathMatch: 'full', 
     component: PagenotfoundComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

