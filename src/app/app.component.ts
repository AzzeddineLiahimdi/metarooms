import { Component } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import {Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  
  constructor(private spinnerService: NgxSpinnerService,public router: Router    ){ 
    this.spinnerService.show("Loading")
      setTimeout(() => {   // <<<---using ()=> syntax
        this.spinnerService.hide("Loading")
      }, 1500);

     
  }
  
    
}
