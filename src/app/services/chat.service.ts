import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { map, switchMap } from 'rxjs/operators';
import { Observable, combineLatest, of } from 'rxjs';
import { arrayUnion } from "firebase/firestore";

@Injectable({
  providedIn: 'root'
})
export class ChatService {
   chat:any;

  constructor(
    private afs: AngularFirestore,
  ) {}

  get(chatId:string) {
    return this.afs
      .collection<any>('chats')
      .doc(chatId)
      .snapshotChanges()
      .pipe(
        map(doc => {
          return { id: doc.payload.id, ...doc.payload.data() };
        })
      );
  }

  async create(uid:string) {
    const data = {
      uid,
      createdAt: Date.now(),
      count: 0,
      messages: []
    };
    const docRef = await this.afs.collection('chats').add(data);
    return docRef.id;
  }

  async sendMessage(chatId:string, content:string,uid:string) {
    let ts=new Date(Date.now());
    const data = {
      uid,
      content,
      createdAt: ts.toLocaleString(),
    };
    if (uid) {
      const ref = this.afs.collection('chats').doc(chatId);
      return ref.update({
        messages: arrayUnion(data)
      });
    }
  }

  joinUsers(chat$: Observable<any>) {
    // @ts-ignore
    let chat;
    const joinKeys =new Map([]);
    return chat$.pipe(
      switchMap(c => {
        // Unique User IDs
        chat = c;
        // @ts-ignore
        const uids = Array.from(new Set(c.messages.map(v => v.uid)));
        // Firestore User Doc Reads
        const userDocs = uids.map(u =>
          this.afs.doc(`users/${u}`).valueChanges()
        );

        return userDocs.length ? combineLatest(userDocs) : of([]);
      }),
      map(arr => {
        // @ts-ignore
        arr.forEach(v => joinKeys.set(v.wallet_address,v));
        // @ts-ignore
        chat.messages = chat.messages.map(v => {
          return { ...v, user: joinKeys.get(v.uid) };
        });
  
      // @ts-ignore
        return chat;
      })
    );
  }
  delete(id?: string): Promise<void> {
    return this.afs.collection('chats').doc(id).delete();
  }
  

}