import { Injectable } from "@angular/core"
import { NFTStorage } from "nft.storage"
const API_KEY = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJkaWQ6ZXRocjoweDcyZUUyNTNBZjc3RTU2MDU0Y2RERkI1NjQyOEE3MTg3Zjk1NURlRDkiLCJpc3MiOiJuZnQtc3RvcmFnZSIsImlhdCI6MTY2Mzc1MTIwNjAzOCwibmFtZSI6ImF6ZGluIn0.rh-VyeXsH9u_WeUCWKNKCa4sJ_POgiVPuVmgEmNyw2s"

@Injectable({
  providedIn: 'root'
})
export class IpfsService {
  public client = new NFTStorage({ token: API_KEY })

  async storeAsset(name:string, description:string,picture:any,video:string,sceneName?:string):Promise<[string, string]> {
    const metadata = await this.client.store({
        name: name,
        description: description,
        image: picture,
        metavers:"https://market.dinomite.click/space/"+sceneName,
        animation_url:video
    })
    var urlmetadata=metadata.url.replace('ipfs://','https://nftstorage.link/ipfs/')
    var urlimage=metadata.data.image.href.replace('ipfs://','https://nftstorage.link/ipfs/')
    console.log(`metadata link: ${urlmetadata}`)
    console.log("image link :"+urlimage)
    return ([urlmetadata,urlimage])
 }
 async storPicture(picture:any):Promise<string>{
  const { car } = await NFTStorage.encodeBlob(picture)
  const cid = await this.client.storeCar(car)
  return(cid)
 }

}
