import { Nft } from '@alch/alchemy-sdk';
import { Injectable, OnDestroy } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { OrderModule } from '../models/order.module';
import { AvatarData, EventData, GameData, MarketOperationData, OrderData, WalletData } from '../models/parameter';
import { Room } from '../models/room';
import { Status } from '../models/status';
import { Voucher, Werabeals } from '../models/werabeals';
import { AlchemyService } from './alchemy.service';
import { ContractsService } from './contracts.service';
import { OrderService } from './firebase-services/order.service';
import { RoomsService } from './firebase-services/rooms.service';
import { LazymintingService } from './lazyminting.service';
import { SpinnerService } from './spinner.service';
import 'src/assets/js/template.js';
import { RoomEventsService } from './firebase-services/room-events.service';
import { PoapService } from './firebase-services/poap.service';
import { AuthService } from './auth.service';
export class PoapGeted {
  UserAddress!: string;
  qr_hash!: string;
}
@Injectable({
  providedIn: 'root'
})
export class UnityService implements OnDestroy {
  public gameInstance: any | undefined;
  public mode = '1';
  public room!: Room;
  public sceneName = ""
  public link = ""
  public asset!: Nft
  public orderStatus!: Status;
  public order!: OrderModule;
  public gameStarted = false;
  public canvas: HTMLElement | undefined;
  public multiplayerMode = false
  public message!: string

  constructor(
    private Lazyservice: LazymintingService,
    private orderService: OrderService,
    private contractsService: ContractsService,
    private alchemyService: AlchemyService,
    private toastr: ToastrService,
    private dbservuce: RoomsService,
    private es: RoomEventsService,
    private spinnerService: SpinnerService,
    private poapserver: PoapService,
    private auth: AuthService,
  ) {
  }
  ngOnDestroy(): void {
  }

  //convert json to instance of eventData class
  static GetTreatmentName(jsonData: any) {
    const eventdata = new EventData(JSON.parse(jsonData));
    return eventdata
  }

  public Treatment(jsonData: any) {
    const eventdata = UnityService.GetTreatmentName(jsonData)
    switch (eventdata.eventName) {
      case "MarketOperation": {
        this.HandleMarketOperation(eventdata);
        break;
      }
      case "Order": {
        this.HandleOrder(eventdata);
        break;
      }
      default: {
        alert("event Name not fond")
        break;
      }
    }
  }

  public async HandleOrder(eventdata: EventData) {
    const parameters = eventdata.parameters
    switch (parameters.orderName) {
      case "GameReady": {
        await this.SendGameData()
        let startEvent = new CustomEvent("enter_ready");
        document.dispatchEvent(startEvent);
        break;
      }
      case "GameStarted": {
        this.gameStarted = true
        this.spinnerService.requestEnded();
        break;
      }
      case "Authentificate": {
        // await this.AuthentificationData()
        this.showError("Authenticate 'order' not implemented")
        break;
      }
      case "SaveRoom": {
        this.SaveRoomData(parameters.parameters[0])
        break;
      }
      case "LockPointer": {
        this.LockPointer()
        break;
      }
      case "VideoUrl": {
        this.OpenModalVideoLink()
        break;
      }
      case "Poap": {
        await this.GetPoap()

        break;
      }
      case "MarketItems": {
        const Wearables = await this.getDCnftsByOwner(this.room.OwnerAddress)
        const order = new OrderData('MarketItems', [JSON.stringify({ nfts: Wearables[1] })])
        eventdata.parameters = order
        await this.SendData(eventdata)
        break;
      }
      default: {
        alert("orderName not fond")
        break;
      }
    }
  }
  public async HandleMarketOperation(eventdata: EventData) {
    const parameters = eventdata.parameters
    switch (parameters.nameOfOperation) {
      case "Sell": {
        const operation = await this.sellWearable(parameters)
        eventdata.parameters = operation
        await this.SendData(eventdata)
        break;
      }
      case "Buy": {
        const operation = await this.lazyMinting(parameters)
        eventdata.parameters = operation
        await this.SendData(eventdata)
        break;
      }
      case "CancelListing": {
        const operation = await this.CancelOrde(parameters)
        eventdata.parameters = operation
        await this.SendData(eventdata)
        break;
      }
      default: {
        alert("order Name not fond")
        break;
      }
    }
  }

  //GameStarted
  async loadGame(isCheckedMode: boolean) {
    this.multiplayerMode = isCheckedMode
    let order = new OrderData('LoadGame', [isCheckedMode.toString()])
    let eventdata = new EventData("Order", order)
    await this.SendData(eventdata)
    //await this.InputCapture("true")
  }
  async InputCapture(value: string) {
    const order = new OrderData('InputCapture', [value])
    const eventdata = new EventData("Order", order)
    await this.SendData(eventdata)
  }
  async sendVideoLink(link: string) {
    this.InputCapture('true')
    const t = link
    const order = new OrderData('VideoUrl', [link])
    const eventdata = new EventData("Order", order)
    await this.SendData(eventdata)
  }

  OpenModalVideoLink() {
    this.InputCapture('false')
    document.getElementById("OpenModalInfo")?.click();
  }

  async GetPoap() {
    this.link = ''
    let address = '';
    address = this.auth.addressUser.getValue();
    if (address != '') {
      const poap = await this.poapserver.HasPoap(address)
      const has = poap[0]
      let poapuser = new PoapGeted()
      const result = await new Promise((resolve, reject) => {
        const sub = this.poapserver.getQr_hash().subscribe(result => {
          resolve(result);
          sub.unsubscribe()
        })
      }) as any[]
      await new Promise(async (resolve, reject) => {
        if (result.length > 0) {
          poapuser.UserAddress = address
          poapuser.qr_hash = result[0].qr_hash
          if (!has) {
            this.poapserver.create(poapuser)
            this.poapserver.removeLink(result[0].id)
            this.message = "poap"
            this.link = "http://POAP.xyz/claim/" + result[0].qr_hash
          } else {
            this.message = "You have already claimed  your POAP"
            this.link = "http://POAP.xyz/claim/" + poap[1]
          }
        } else {
          this.message = "Sorry, all available POAPs have been collected."
        }
        document.getElementById("OpenModalInfo2")?.click();
      })
    } else {
      this.message = "Connect your wallet before using the POAP machine"
      document.getElementById("OpenModalInfo2")?.click();
    }

  }
  async PoapGeted() {
    const order = new OrderData('Poap', [""])
    const eventdata = new EventData("Order", order)
    await this.SendData(eventdata)
  }


  async SendWalletData() {
    let userAddress = this.auth.addressUser.getValue();
    if (userAddress != '') {
      const result = await this.getDCnftsByOwner(userAddress);
      let walletData = new WalletData(userAddress, result[1], [], [], [], [])
      let eventdata = new EventData("WalletData", walletData)
      await this.SendData(eventdata)
    }
  }

  public async SendAvatarData(name: string, type: string, url: string) {
    let avatarData = new AvatarData(name, type, url)
    let eventdata = new EventData("AvatarData", avatarData)
    await this.SendData(eventdata)
  }
  public async SendGameData() {
    let gameData = new GameData(
      this.mode,
      this.sceneName,
      this.room)
    const eventdata = new EventData("GameData", gameData)
    await this.SendData(eventdata)
  }

  //Authentificate
  async AuthentificationData() {
    if (!this.auth.loginUser) await this.auth.connect();
    let userAddress = ""
    this.auth.addressUser.subscribe((res: string) => {
      if (res != '') {
        userAddress = res;
      }
      const order = new OrderData('Authentificate', [userAddress])
    })

    //await this.SendData(order)
  }
  //SaveRoom
  SaveRoomData(jsonData: string) {
    if (this.room.id != undefined) {
      try {
        this.dbservuce.Update(this.room.id, jsonData);
        this.showSuccess('Room saved');
      } catch (error) {
        this.showError('Data not saved');
      }
    }
  }
  LockPointer() {
    if (this.canvas) {
      this.canvas.requestPointerLock();
    } else {
      alert("no canvas")
    }

  }
  async Unlockponter() {
    const order = new OrderData('ExitGame', [])
    const eventdata = new EventData("Order", order)
    await this.SendData(eventdata)
  }
  async getDCnftsByOwner(address: string) {
    const result = await this.alchemyService.getDCnftsByAddress(address)
    return result;
  }
  public async SendData(data: EventData) {
    data.parameters = JSON.stringify(data.parameters)
    const dataToSend = JSON.stringify(data)
    await this.gameInstance.SendMessage('ReactConnector', 'CallConnectorEvent', dataToSend);
  }
  public async SendData2(data: EventData) {
    const dataToSend = JSON.stringify(data)
    await this.gameInstance.SendMessage('ReactConnector', 'CallConnectorEvent', dataToSend);
  }
  async sellWearable(operation: MarketOperationData) {
    const data = operation.dataOfNft
    let address = this.auth.addressUser.getValue();
    let owner = await this.contractsService.getOwner(data.nft.contractAddress, data.nft.tokenId);
    let isApproved = false
    let isAdded = false
    try {
      this.asset = await this.alchemyService.getMetadata(data.nft.contractAddress, data.nft.tokenId.toString())
      if (owner.toLowerCase() == address) {
        this.orderStatus = await this.orderService.getOrderByContract(data.nft.contractAddress, data.nft.tokenId.toString())
        isApproved = await this.contractsService.Approve(data.nft.contractAddress, address, 1)
        if (isApproved && !this.orderStatus.isListed) {
          isAdded = await this.Lazyservice.AddSignature(
            this.asset?.tokenUri?.raw,
            Number(Number(data.price).toFixed(6)), 1, 1, 0,
            data.nft.tokenId, address,
            data.nft.contractAddress,
            this.asset.rawMetadata!.image!,
            this.asset.title,
            this.asset.rawMetadata!.attributes![1]['value']!)
          data.isListed = isAdded
          operation.result.iSucceed = isAdded
          this.showSuccess('Wearable on sale');
        } else {
          data.isListed = isAdded
          operation.result.iSucceed = isAdded
          operation.result.problem = "wearables already on sale"
          this.showError("wearables already on sale");
        }
      } else {
        data.isListed = isAdded
        operation.result.iSucceed = isAdded
        operation.result.problem = "you are not the owner of this wearable"
        this.showError("you are not the owner of this wearable");
      }
    } catch (error) {
      data.isListed = false
      operation.result.iSucceed = isAdded
      operation.result.problem = "Sale canceled"
      this.showError("Sale canceled");
      console.log(error)
    }
    operation.dataOfNft = data
    return operation
  }
  async lazyMinting(operation: MarketOperationData) {
    const nftDta = operation.dataOfNft
    //test if the user is logged in with metamask
    const flag = this.auth.loginUser.getValue()
    //GET THE ADDRESS OF USER
    let address = this.auth.addressUser.getValue();

    //GET ORDER FROM BD 
    this.order = await this.orderService.getOrderContract(nftDta.nft.contractAddress, nftDta.nft.tokenId)
    // CREATION OF THE VOUCHER SIGNED BY THE SELLER
    const voucher = new Voucher(nftDta, this.order)
    let sold = false
    try {
      if (flag) {
        sold = await this.Lazyservice.lazyMinting(address, this.order.id, voucher, 1);
        nftDta.isListed = !sold
        operation.result.iSucceed = sold
        if (sold) {
          this.showSuccess('Wearable sold');
          this.addEventMessage(this.room.eventsId, nftDta.nft, address)
        }
      } else {
        operation.result.iSucceed = sold
        operation.result.problem = "Wallet not connected"
        this.showError("Wallet not connected");
      }
    } catch (error) {
      operation.result.iSucceed = false
      operation.result.problem = "Buy canceled"
      this.showError('Buy canceled')
    }
    operation.result.problem = "Buy canceled"
    operation.dataOfNft = nftDta
    return operation
  }

  async CancelOrde(operation: MarketOperationData) {
    const nftDta = operation.dataOfNft
    try {
      this.order = await this.orderService.getOrder(nftDta.nft.contractAddress, nftDta.nft.tokenId)
      if (this.order.id != undefined) {
        this.orderService.removeOrder(this.order.id)
        nftDta.isListed = false
        operation.result.iSucceed = true
      } else {
        nftDta.isListed = true
        operation.result.iSucceed = false
        operation.result.problem = "there is no order with the given data"
        this.showError('there is no order with the given data')
      }
      nftDta.isListed = false
      operation.result.iSucceed = true
      this.showSuccess('Order cancelled')
    } catch (error) {
      nftDta.isListed = false
      operation.result.iSucceed = false
      operation.result.problem = "order already canceled"
      this.showError('order already canceled')
    }
    operation.dataOfNft = nftDta
    return operation
  }

  showSuccess(message: string) {
    this.toastr.success(message, 'Success', {
      timeOut: 6000,
      positionClass: "toast-bottom-right",
    });
  }
  showError(message: string) {
    this.toastr.error(message, 'Error', {
      timeOut: 6000,
      positionClass: "toast-bottom-right",
    });
  }
  async addEventMessage(eventId: string, nft: Werabeals, address: string) {
    let name = await this.alchemyService.getNftMetadata(nft.contractAddress, nft.tokenId)
    this.es.addEventMessage(eventId, name?.rawMetadata?.name + " was bought", "Buy", address);
    setTimeout(() => {
      this.es.deleteMessage(eventId)
    }, (60));
  }
}
