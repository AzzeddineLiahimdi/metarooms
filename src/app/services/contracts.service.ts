import { Injectable } from '@angular/core';
import detectEthereumProvider from '@metamask/detect-provider';
import { ethers } from 'ethers';
import { environment } from 'src/environments/environment';
import abi from '../models/ERC721abi.json'
import templateabi from 'src/artifacts/contracts/Collection.sol/Collection.json';
import NFTMarket from 'src/artifacts/contracts/Marketplace.sol/Marketplace.json';
import dclabi from '../models/dclStore.json'

@Injectable({
  providedIn: 'root'
})
export class ContractsService {
  providre: ethers.providers.JsonRpcProvider | undefined;

  constructor() {
    this.providre = new ethers.providers.JsonRpcProvider(environment.API_URL)
  }

  private static async getWebProvider(requestAccounts = true) {
    const provider: any = await detectEthereumProvider()
    if (requestAccounts) {
      await provider.request({ method: 'eth_requestAccounts' })
    }
    return new ethers.providers.Web3Provider(provider)
  }

  async getOwner(contract: string, tokenId: string): Promise<string> {
    const collection = new ethers.Contract(contract, abi, this.providre);
    const owner = await collection['ownerOf'](tokenId);
    return owner
  }

  async Buy(contract: string, itemId: number,price:number,user:string){
    const ContractDcl = await ContractsService.getDCLcontract(true)
    const priceGWEI = ethers.utils.parseUnits(price.toString(), 'ether')
    const itemsToBuy=[[contract,[itemId],[priceGWEI],[user]]]
    console.log("itemsToBuy",itemsToBuy)
    let status=0
    try {
      let transaction = await ContractDcl['buy'](itemsToBuy);
      let tx = await transaction.wait()
      status=tx.status
    } catch (error:any) {
      console.log(error?.message)
    }    
      return status==1
  }
  private static async getDCLcontract(bySigner = false) {
    const provider = await ContractsService.getWebProvider()
    const signer = provider.getSigner()
    return new ethers.Contract(
      environment.dclStore,
      dclabi,
      bySigner ? signer : provider,
    )
  }

  public async Approve(nftcontract: string, address?: string, standart?: number): Promise<boolean> {
    let tx;
    let isapprove = false;
    console.log(nftcontract, address, standart)
    try {
      const contract = await ContractsService.getContractnft(standart, nftcontract, true)
      const isApprove = await contract['isApprovedForAll'](address, environment.NFTMARKET_CONTRACT_ADDRESS);
      isapprove = isApprove;
      if (!isApprove) {
        let transaction = await contract['setApprovalForAll'](environment.NFTMARKET_CONTRACT_ADDRESS, true);
        tx = await transaction.wait();
        isapprove = tx.status === 1;
      }
    } catch (error) {
      console.log("operation cancellation", error)
    }
    return isapprove
  }
  public async setApprovalForAll(conntractTo:string,standartNFT?: number){
    const contract = await ContractsService.getContractnft(standartNFT, environment.TEMPLATE8CONTRACT, true)
    let transaction =  await contract['setApprovalForAll'](conntractTo, true);
    let tx = await transaction.wait();
    return(tx.status === 1)
  }
  public async isApprovedForAll(address: string,conntractTo:string){
    const contract = await ContractsService.getContractnft(2, environment.TEMPLATE8CONTRACT, true)
      const isApprove = await contract['isApprovedForAll'](address, conntractTo);
      return(isApprove)

  }

  private static async getContractnft(standard?: number, contractaddress?: any, bySigner = false) {
    const provider = await ContractsService.getWebProvider()
    const signer = provider.getSigner()
    let abiContract
    if (standard == 1) {
      abiContract = abi
    } else {
      abiContract = templateabi.abi
    }
    return new ethers.Contract(
      contractaddress,
      abiContract,
      bySigner ? signer : provider,
    )
  }
  private static async getContract(bySigner = false) {
    const provider = await ContractsService.getWebProvider()
    const signer = provider.getSigner()

    return new ethers.Contract(
      environment.TEMPLATE8CONTRACT,
      templateabi.abi,
      bySigner ? signer : provider,
    )
  }

  //templates

  async mintTemplate(amount: number, tokenURI: string, royalties: number) {
    let tx
    let tokenId
    try {
      const collection = await ContractsService.getContract(true)
      let transaction = await collection['addNewNFT'](amount, tokenURI, royalties);
      tx = await transaction.wait()
      let event = tx.events[0]
      let value = event.args[3]
      tokenId = Number(value)
      console.log("status", tokenId, tx)
    } catch (error) {
      console.log(error)
    }
    if (tx.status == 1) {
      return tokenId
    } else {
      return 0
    }

  }
  //withdraw
  private static async getMarketContract(bySigner = false) {
    const provider = await ContractsService.getWebProvider()
    const signer = provider.getSigner()

    return new ethers.Contract(
      environment.NFTMARKET_CONTRACT_ADDRESS,
      NFTMarket.abi,
      bySigner ? signer : provider,
    )
  }
  public async getContractBalance(): Promise<String> {
    let contractn = await ContractsService.getMarketContract(true);
    let data = "";
    try {
      const Balance = await contractn['getContractBalance']();
      data = (Balance / 1000000000000000000).toString();
    } catch (error: any) {
      console.log(error.message)
    }
    return data
  }
  public async withdraw(to: string) {
    let contractn = await ContractsService.getMarketContract(true)
    let transaction = await contractn['withdraw'](to);
  }
  ///assets
  async getaddressBalance(address?:string,token?:number){ 
    const collection = new ethers.Contract(environment.TEMPLATE8CONTRACT, templateabi.abi, this.providre);
    const balance = await collection['balanceOf'](address,token);
    return balance
  }
  public async getMetaData(contracttype: number, nftContract: string, token: number): Promise<string> {
    const provider = new ethers.providers.JsonRpcProvider(environment.API_URL);
    let TokenContract: ethers.Contract;
    let uri: string;

    let abiContract
    if (contracttype == 1) {
      abiContract = abi
    } else {
      abiContract = templateabi.abi
    }

    if (contracttype == 1) {
      TokenContract = new ethers.Contract(nftContract, abiContract,provider)
      uri = await TokenContract['tokenURI'](token);
    } else {
      TokenContract = new ethers.Contract(nftContract, abiContract,provider)
      uri = await TokenContract['uri'](token);
    }
    return uri
  }

}
