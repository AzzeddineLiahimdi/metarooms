import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import detectEthereumProvider from "@metamask/detect-provider";
import swal from 'sweetalert2';
import { ethers } from "ethers";
declare let window: any

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  provider: any = null;
  get WebProvider() { return this.provider; };
  chainIds: string[] = ['0x89','0x1'];
  addressUser: any = new BehaviorSubject<string>('');
  loginUser: any = new BehaviorSubject<boolean>(false)

  constructor() {
    if (typeof window.ethereum !== 'undefined') {
      window.ethereum.request({ method: 'eth_accounts' }).then(async (result: any) => {
        if (result.length > 0 && localStorage.getItem("isConnected") === "true") {
          this.connect()
        }
      });
      this.getWebProvider().then(async (result: any) => {
        this.provider = result
      })
    } else {
      if (/iPhone|iPad|iPod|Android/i.test(navigator.userAgent)) {
        this.MobileAlert()
      }else{
       this.InstallMetaMaskAlert() 
      }
      
    }
  }
  async connect() {
    if (typeof window.ethereum !== 'undefined') {
      this.handleIdChainChanged()
    } else {
      this.InstallMetaMaskAlert()
    }
  }
  
  async handleIdChainChanged() {
    const chainId: string = await window.ethereum.request({ method: 'eth_chainId' });
    if (this.chainIds.includes(chainId)) {
      this.handleAccountsChanged();
    } else {
      this.SupportedNetworkAlert()
    }
    window.ethereum.on('chainChanged', (res: string) => {
      if (!this.chainIds.includes(res)) {
        this.logout();
        this.SupportedNetworkAlert()
      } else {
        if (this.addressUser.getValue() == '') {
          this.handleAccountsChanged();
        } else {
          this.authBackend();
        }
      }
    })
  }
  async handleAccountsChanged() {
    try {
      const accounts: string[] = await window.ethereum.request({ method: 'eth_requestAccounts' });
      this.addressUser.next(accounts[0])
      this.authBackend();
      window.ethereum.on('accountsChanged', (accounts: string[]) => {
        this.addressUser.next(accounts[0]);
        if (accounts.length > 0) {
          this.authBackend();
        } else {
          this.logout()
        }
      })

    } catch (error: any) {
      if (error.code === -32002) {
        this.ChekWalletAlert()
      }
    }
  }
  async authBackend() {
    this.loginUser.next(true)
    localStorage.setItem("isConnected", 'true');
  }
  async logout() {
    this.loginUser.next(false)
    localStorage.setItem("isConnected", 'false');
  }

  public async getWebProvider(): Promise<any> {
    const provider: any = await detectEthereumProvider()
    //await provider.request({ method: 'eth_requestAccounts' })
    return new ethers.providers.Web3Provider(provider)
  }
  
  public async getBalance(account: string): Promise<number | undefined> {
    return window.ethereum.request({ method: 'eth_getBalance', params: [account, 'latest'] })
      .then((balance: number | undefined) => {
        return (Number(balance) / Math.pow(10, 18)).toFixed(3);
      }).catch((error: { message: any; }) => {
        console.log(error.message);
      });
  }

  ChekWalletAlert() {
    swal.fire({
      icon: 'warning',
      timer: 3000,
      title: '<div style="margin: 0 0 -5px !important;padding: 0 !important;">Metamask is already open, check your wallet</div>'
    })
  }
  InstallMetaMaskAlert() {
    swal.fire({
      imageUrl: '../../../assets/img/metamask.gif',
      imageWidth: 80,
      imageHeight: 70,
      title: "<div style='margin: 0 0 -5px !important;padding: 0 !important;'>You don't have metamask installed</div>",
      confirmButtonText: '<div style="width: 15em;">Install Metamask</div>',
      confirmButtonColor: '#3085d6',
      showCancelButton: true,
      cancelButtonText:"<div style='width: 15em;'>No thanks</div>",
      allowOutsideClick: false,
      width: 600,
    }).then((result) => {
      if (result.isConfirmed) {
        window.open("https://metamask.io/");
      }
    })
  }
  MobileAlert() {
    swal.fire({
      imageUrl: '../../../assets/img/metamask.gif',
      imageWidth: 80,
      imageHeight: 70,
      title: "<div style='margin: 0 0 -5px !important;padding: 0 !important;'>Open the website on the metamask browser to be able to interact with the blockchain.</div>",
      confirmButtonText: '<div style="width: 15em;">Open</div>',
      confirmButtonColor: '#3085d6',
      showCancelButton: true,
      cancelButtonText:"<div style='width: 15em;'>No thanks</div>",
      allowOutsideClick: false,
      width: 600,
    }).then((result) => {
      if (result.isConfirmed) {
        const url = "https://metamask.app.link/dapp/market.dinomite.click/homepage";
        window.location.replace(url);
      }
    })
  }
  SupportedNetworkAlert() {
    swal.fire({
      icon: 'warning',
      title: "<div style='margin: 0 0 -5px !important;padding: 0 !important;'>Partially Supported Network</div>",
      text: "You need to be connected to Polygon Mainnet to use all the features of this app. therefore, all transactions that occur on another network will be rolled back.",
      confirmButtonText: '<div style="width: 15em;">Switch to polygon Mainnet</div>',
      showCancelButton: true,
      cancelButtonText:"<div style='width: 15em;'>No thanks</div>",
      allowOutsideClick: false,
      confirmButtonColor: '#3085d6',
      width: 700,
    }).then(async (result) => {
      if (result.isConfirmed) {
        try {
          await window.ethereum.request({
            method: 'wallet_switchEthereumChain',
            params: [{ chainId: '0x89' }],
          });
        } catch (switchError: any) {
          if (switchError.code === 4902) {
            try {
              await window.ethereum.request({
                method: 'wallet_addEthereumChain',
                params: [
                  {
                    chainId: '0x89',
                    chainName: 'Polygon Mainnet',
                    rpcUrls: ['https://polygon-rpc.com/'] /* ... */,
                    nativeCurrency: {
                      name: 'Polygon',
                      symbol: 'MATIC',
                      decimals: 18,
                    },
                  },
                ],
              });
            } catch (Error: any) {
              if (Error.code === -32002) {
                this.ChekWalletAlert()
              }
            }
          } else if (switchError.code === -32002) {
            this.ChekWalletAlert()
          }
        }
      }
    })
  }
  async switchNetwork(chainid:string,name:string){
    const chainId: string = await window.ethereum.request({ method: 'eth_chainId' });
    if (chainId==chainid) {
      return true
    } else {
    swal.fire({
      icon: 'warning',
      title: "<div style='margin: 0 0 -5px !important;padding: 0 !important;'>Partially Supported Network</div>",
      text: "You need to be connected to "+name+" Mainnet to complite this action.",
      confirmButtonText: '<div style="width: 15em;">Switch to '+name+' Mainnet</div>',
      showCancelButton: true,
      cancelButtonText:"<div style='width: 15em;'>No thanks</div>",
      allowOutsideClick: false,
      confirmButtonColor: '#3085d6',
      width: 700,
    }).then(async (result) => {
      if (result.isConfirmed) {
        try {
          await window.ethereum.request({
            method: 'wallet_switchEthereumChain',
            params: [{ chainId: chainid }],
          });
        } catch (switchError: any) {
          this.ChekWalletAlert()
        }
      }
    }) 
    return false
  }
  }
  isMetaMaskInstalled(): boolean {
    return Boolean(window.ethereum && window.ethereum.isMetaMask);
  }

  async switchChain(chainid:string){
    try {
      await window.ethereum.request({
        method: 'wallet_switchEthereumChain',
        params: [{ chainId: chainid }],
      });
    } catch (switchError: any) {
      console.log(switchError)
    }
  }
}
