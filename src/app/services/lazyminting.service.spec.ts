import { TestBed } from '@angular/core/testing';

import { LazymintingService } from './lazyminting.service';

describe('LazymintingService', () => {
  let service: LazymintingService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(LazymintingService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
