import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { DclNFT } from 'src/app/models/decl/dclnft';
import { map } from 'rxjs';
import { ParamsDcl } from 'src/app/models/decl/params';
import { forkJoin } from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class GetDataService {

  constructor(private http: HttpClient) { }
  getTrendings() {
    const params = new HttpParams().set("size", "20");
    return this.http.get('https://nft-api.decentraland.org/v1/trendings', { params }).pipe(
      map((data: any) => { return data.data.filter((item: DclNFT) => item.category === "wearable") }),
      map((datas: DclNFT[]) => datas.map(response => ({ ...response, price: Number(response.price) / Math.pow(10, 18) })))
    )
  }
  getNewests() {
    const params = new HttpParams().set("first", "15").set("sortBy", "recently_reviewed").set("category", "wearable").set("isOnSale", "true");
    return this.http.get('https://nft-api.decentraland.org/v1/items', { params }).pipe(
      map((data: any) => { return data.data.filter((item: DclNFT) => item.category === "wearable") }),
      map((datas: DclNFT[]) => datas.map(response => ({ ...response, price: Number(response.price) / Math.pow(10, 18) }))))
  }
  getPopularCreators() {
    const params = new HttpParams().set("orderBy", "followersCount").set("userType", "CREATOR").set("sortOrder", "desc").set("take", "9");
    return this.http.get('https://api.droffo.com/creator/find', { params }).pipe(
      map((data: any) => { return data.creators }),
    )
  }
  getNewcreators() {
    const params = new HttpParams().set("orderBy", "createdAt").set("sortOrder", "asc").set("take", "9");
    return this.http.get('https://api.droffo.com/creator', { params })
  }
  getNewestsCollection() {
    const params = new HttpParams().set("first", "12").set("sortBy", "recently_reviewed");
    return this.http.get('https://nft-api.decentraland.org/v1/collections', { params }).pipe(
      map((data: any) => { return data.data }),
    )
  }
  getAdidasCollection() {
    return this.http.get('https://api.rarible.org/v0.1/collections/ETHEREUM:0xba0c9cf4da821dba98407cc4f9c11f6c7a5f9bbc')
  }
  getCollections() {
    return forkJoin([
      //this.http.get('https://api.rarible.org/v0.1/collections/POLYGON:0x4bb5b2461e9ef782152c3a96698b2a4cf55b6162'),
      this.http.get('https://api.rarible.org/v0.1/collections/ETHEREUM:0xba0c9cf4da821dba98407cc4f9c11f6c7a5f9bbc'),
      this.http.get('https://api.rarible.org/v0.1/collections/ETHEREUM:0xa909f1825f97434e816398bd4901b94f409b6c5d'),
      this.http.get('https://api.rarible.org/v0.1/collections/ETHEREUM:0x7c120e2159fec5e76eb97b43685ab89bfb8c71d0'),
      this.http.get('https://api.rarible.org/v0.1/collections/ETHEREUM:0x08977d2f3f3a38240bed7f9b0342d20e66906347'),
      this.http.get('https://api.rarible.org/v0.1/collections/POLYGON:0x88359fac16f41efc4a74e7fe639879df1263928b')
    ])
  }
  getcollection(address:string,netowrk:string){
    return this.http.get('https://api.rarible.org/v0.1/collections/'+ netowrk+':'+address)
  }
  getAdidasItems(netowrk:string,collection:string,loadMore = '') {
    let params = new HttpParams().set("collection", netowrk+":"+collection).set("size", 24);
    if (loadMore != '') {
      params = params.set("continuation", loadMore)
    }
    return this.http.get('https://api.rarible.org/v0.1/items/byCollection', { params }).pipe(
      map((data: any) => { return data }),
    )
  }
  getRaribleItemsByCollection(collection: string, Network: string, loadMore = '') {
    let params = new HttpParams().set("collection", Network + ":" + collection).set("size", 24);
    if (loadMore != '') {
      params = params.set("continuation", loadMore)
    }
    return this.http.get('https://api.rarible.org/v0.1/items/byCollection', { params }).pipe(
      map((data: any) => { return data }),
    )
  }
  getOrder(tokenId: string): Promise<any[]> {
    const params = new HttpParams().set("itemId", "ETHEREUM:0xba0c9cf4da821dba98407cc4f9c11f6c7a5f9bbc:" + tokenId).set("status", "ACTIVE");
    return new Promise((resolve, reject) => {
      this.http.get(' https://api.rarible.org/v0.1/orders/sell/byItem', { params }).pipe(
        map((data: any) => { return data.orders }),
      ).subscribe(data => {
        resolve(data);
      })
    })
  }
  getTestpOST() {
    const headerDict = {
      'Content-Type': 'application/json',

    }
    const requestOptions = {
      headers: new HttpHeaders(headerDict),
    };

    const data = JSON.stringify({
      "size": 50,
      "continuation": "string",
      "filter": {
        "blockchains": [
          "ETHEREUM"
        ],
        "collections": [
          "ETHEREUM:0xb66a603f4cfe17e3d27b87a8bfcad319856518b8"
        ],
        "deleted": "false",
        "names": [
          "MutantApeYachtClub"
        ],
        "descriptions": [
          "MUTANT"
        ],
        "traits": [
          {
            "key": "Hat",
            "value": "Halo"
          }
        ],
        "creators": [
          "ETHEREUM:0x4765273c477c2dc484da4f1984639e943adccfeb"
        ],
        "mintedAtFrom": "2021-08-25T00:00:00Z",
        "mintedAtTo": "2029-12-01T00:00:00Z",
        "lastUpdatedAtFrom": "2021-08-25T00:00:00Z",
        "lastUpdatedAtTo": "2029-12-01T00:00:00Z",
        "sellPriceFrom": "0",
        "sellPriceTo": "999999",
        "sellCurrency": "ETHEREUM:0x0000000000000000000000000000000000000000",
        "sellPlatforms": [
          "RARIBLE"
        ],
        "bidPriceFrom": "0",
        "bidPriceTo": "999999",
        "bidCurrency": "ETHEREUM:0x0000000000000000000000000000000000000000",
        "bidPlatforms": [
          "RARIBLE"
        ]
      },
      "sort": "LATEST"
    });
    return this.http.post('https://rarible.com/marketplace/api/v4/items/search', data, requestOptions);
  }
  getOrders(network:string,collection:string,tokenId: string) {
    const params = new HttpParams().set("itemId", network+":"+collection+":" + tokenId).set("status", "ACTIVE");
    return this.http.get(' https://api.rarible.org/v0.1/orders/sell/byItem', { params }).pipe(
      map((data: any) => { return data.orders }),
    )
  }
  getitemsBycollection(collection: string) {
    const params = new HttpParams().set("category", "wearable").set("contractAddress", collection);
    return this.http.get('https://nft-api.decentraland.org/v1/items', { params }).pipe(
      map((data: any) => { return data.data }),
      map((datas: DclNFT[]) => datas.map(response => ({ ...response, price: Number(response.price) / Math.pow(10, 18) })))
    )
  }
  getItem(collection: string, itemId: number) {
    const params = new HttpParams().set("contractAddress", collection).set("itemId", itemId);
    return this.http.get('https://nft-api.decentraland.org/v1/items', { params }).pipe(
      map((data: any) => { return data.data[0] }),
      map(response => ({ ...response, price: Number(response.price) / Math.pow(10, 18) }))
    )
  }

  Browse(paramsdcl: ParamsDcl, loadMore = 0) {
    //first=24&skip=24&sortBy=recently_reviewed&category=wearable&isOnSale=true
    let params = new HttpParams()
      .set("first", "24")
      .set("sortBy", "recently_reviewed")
      .set("category", "wearable")
      .set("isOnSale", "true");
    if (loadMore != 0) {
      params = params.set("skip", loadMore)
    }
    if (paramsdcl.category && paramsdcl.category != "all") {
      if (paramsdcl.category == "isWearableHead" || paramsdcl.category == "isWearableAccessory") {
        params = params.set(paramsdcl.category, "true")
      } else {
        params = params.set("wearableCategory", paramsdcl.category)
      }
    }
    if (paramsdcl.sex && paramsdcl.category != "all") {
      params = params.set("wearableGender", paramsdcl.sex)
    }
    if (paramsdcl.rarity && paramsdcl.rarity != "all") {
      params = params.set("rarity", paramsdcl.rarity)
    }
    return this.http.get('https://nft-api.decentraland.org/v1/items', { params }).pipe(
      map((data: any) => { return data.data }),
      map((datas: DclNFT[]) => datas.map(response => ({ ...response, price: Number(response.price) / Math.pow(10, 18) })))
    )
  }
  GetMANAbalance(address: string) {
    return this.http.get('https://api.droffo.com/trading/balance/' + address + '/MANA')
  }
  getOpenSea() {
    return this.http.get("https://api.opensea.io/api/v1/assets?asset_contract_addresses=0xba0c9cf4da821dba98407cc4f9c11f6c7a5f9bbc")
  }
  getItembyId(itemId:string){
    return this.http.get('https://api.rarible.org/v0.1/items/'+ itemId )
  }


}
