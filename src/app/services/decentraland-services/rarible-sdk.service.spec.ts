import { TestBed } from '@angular/core/testing';

import { RaribleSdkService } from './rarible-sdk.service';

describe('RaribleSdkService', () => {
  let service: RaribleSdkService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(RaribleSdkService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
