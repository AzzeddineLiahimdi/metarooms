import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/compat/firestore';
export class PoapGeted {
  UserAddress!:string;
  qr_hash!:string;
}

@Injectable({
  providedIn: 'root'
})
export class PoapService {

  private dbPath = '/PoapGeted';
  tempRef: AngularFirestoreCollection<PoapGeted>;

  constructor(private db: AngularFirestore) {
    this.tempRef = db.collection(this.dbPath);
  }
  async create(poap: PoapGeted) {
    console.log(poap)
    return this.tempRef.add({ ...poap})
  }
   getQr_hash() {
    return this.db.collection('/poap',ref => ref.limit(1)).valueChanges({ idField: 'id' })
  }
  async HasPoap(address?:string):Promise<any>{
    return new Promise((resolve, reject) => {
      const sup= this.db.collection(this.dbPath, ref => ref.where("UserAddress", "==", address)).valueChanges({ idField: 'id' }).subscribe(result => {
      if(result.length > 0){
        const poap=result as any
        resolve([true,poap[0].qr_hash]);
      }else{
        resolve([false,""]);
      }
       sup.unsubscribe()
     })
    })
   }
   removeLink(id?: string) {
    return this. db.collection("poap").doc(id).delete();
  }

}
