import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/compat/firestore';
import {Room} from 'src/app/models/room'
import { User } from 'src/app/models/user';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  private dbPath = '/users';
marketItemsRef: AngularFirestoreCollection<Room>;
constructor(private db: AngularFirestore) {
  this.marketItemsRef = db.collection(this.dbPath);
}
getAll(): AngularFirestoreCollection<Room> {
  return this.marketItemsRef;
}
async login(address: string) {
  return new Promise((resolve, reject) => {
    this.db.doc("users" + `/${address}`)
      .get().subscribe((one) => {
        if (one.data() == undefined) {
          this.createUser(address);
          console.log("account created")
        } else {
          console.log("connect")
        }
      }
      )
  })
}
async createUser(address: string) {
  const data = {
    username: "unknown",
    wallet_address: address,
    profile_image: "assets/img/photo2.png",
    banner_image: "assets/img/Content-writing-Banner.png",
    email: "unknown@gmail.com",
  }
  // Add a new document in collection "cities" with ID 'address'
  const res = await this.db.collection('users').doc(address).set(data);
}
updateUser(id: string, username: string, email: string, profileImage?: string, bannerImmage?: string) {
  return this.db.collection("/users").doc(id).update({
    email: email,
    username: username,
    banner_image: bannerImmage,
    profile_image: profileImage
  });
}
getUser(id: string) {
  return this.db.collection('/users').doc(id).valueChanges();
}
async getuser(address:string) {
  return new Promise((resolve, reject) => {
    this.db
      .collection("users", ref => ref.where("wallet_address", "==",address)).valueChanges({ idField: 'id' }).subscribe(result=>{
        resolve( result[0]);
      })
  })
}
getDataUser(id: string): AngularFirestoreCollection<User> {
  return this.db.collection('/users', ref => ref.where("wallet_address", "==", id))
}

getLastUsers(): AngularFirestoreCollection<Room>{
  return this.db.collection(this.dbPath, ref => ref.limit(12));
}



}
