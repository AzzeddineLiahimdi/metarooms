import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/compat/firestore';
import { serverTimestamp } from 'firebase/firestore';
import { OrderModule } from 'src/app/models/order.module'
import { Status } from 'src/app/models/status';
@Injectable({
  providedIn: 'root'
})
export class OrderService {

  private dbPath = '/listings';
  marketItemsRef: AngularFirestoreCollection<OrderModule>;
  data: any[] = [];
  nft:any

  constructor(private db: AngularFirestore,) {
    this.marketItemsRef = db.collection(this.dbPath);
  }
  getAll(): AngularFirestoreCollection<OrderModule> {
    return this.marketItemsRef;
  }
  getAll2() {
    return this.db.collection('/listings').valueChanges({ idField: 'id' })
  }
  /*
  async getAllwearables() {
    let i = 0
    return new Promise((resolve, reject) => {
      this.db
        .collection("listings", ref => ref.where("Category", "!=","Template"))
        .get()
        .subscribe((snapshot: any) => {
          snapshot.docs.forEach((doc: any) => {
            this.db.collection('/users').doc(doc.data().OwnerAddress).get().subscribe((user) => {
              this.data[i] = {
                wearable: doc.data(),
                seller: user.data()
              }
              i++
            })
            // console.log(doc.data())
          });
          resolve(this.data);
        })
    })
  }*/
  async getOrderByContract(contract?: string, tokenId?: string):Promise<Status> {
    let state={
      isListed:false,
      price:'0'
    }
    return new Promise((resolve, reject) => {
     const sup= this.db
        .collection("/listings", ref => ref.where("contractAddress", "==", contract).where("tokenId", "==", tokenId)).valueChanges({ idField: 'id' }).subscribe(result => {
         if(result.length > 0){
          this.nft=result
          state={
          isListed:true,
          price:this.nft[0].price
         }
         }
          resolve(state as Status);
          sup.unsubscribe()
        })
    })
  }
  getOrderByitemId(contract?: string, tokenId?: string) {
    return this.db.collection('/listings', ref => ref.where("contractAddress", "==", contract).where("tokenId", "==", tokenId)).valueChanges({ idField: 'id' })
  }
  getOrderContract(contract?: string, tokenId?: string):Promise<OrderModule> {
    return new Promise((resolve, reject) => {
      this.db.collection('/listings', ref => ref.where("contractAddress", "==", contract).where("tokenId", "==", tokenId)).valueChanges({ idField: 'id' }).subscribe(result =>{
        resolve(result[0] as OrderModule);
      })
      })
  }
  getLastOrder(itemId: string) {
    return this.db.collection(this.dbPath, ref => ref.where("itemId", "==", itemId).limit(1)).valueChanges({ idField: 'id' })
  }
  getOrderBySeller(seller: string) {
    return this.db.collection(this.dbPath, ref => ref.where("OwnerAddress", "==", seller)).valueChanges({ idField: 'id' })
  }

  createOrder(price: Number, amounttosall: number, seller: string, contractAddress: string, metadata: string, royalties: number, tokenId: number, signature: string, image: string, title: string, nonce: number, Category: string) {
    const timestamp = serverTimestamp;
    const emotes=["dance","poses","fun","greetings","","Horror","Miscellaneous","Stunt","Reactions"]
    let type="wearable"
    if(emotes.includes(Category)){
      type="emote"
    }else if(Category=="Template"){
      type=Category
    }
    return this.db.collection('listings').add({
      price: price,
      amounttosall: amounttosall,
      OwnerAddress: seller,
      contractAddress: contractAddress,
      metadata: metadata,
      royalties: royalties,
      tokenId: tokenId.toString(),
      signature: signature,
      image: image,
      title: title,
      nonce: nonce,
      Category: Category,
      createdAt:timestamp(),
      type:type
    });
  }
  update(id: string, data: any): Promise<void> {
    return this.marketItemsRef.doc(id).update(data);
  }
  removeOrder(id?: string): Promise<void> {
    return this.marketItemsRef.doc(id).delete();
  }
  getOrder(contract?: string, tokenId?: string): Promise<OrderModule> {
    return new Promise((resolve, reject) => {
       this.db.collection("/listings", ref => ref.where("contractAddress", "==", contract).where("tokenId", "==", tokenId)).valueChanges({ idField: 'id' }).subscribe(result=>{
        resolve(result[0] as OrderModule)
       })})
  }
  getSpaceById(id:string){
    return this.db.collection(this.dbPath).doc(id).valueChanges({ idField: 'id' });
   }

}
