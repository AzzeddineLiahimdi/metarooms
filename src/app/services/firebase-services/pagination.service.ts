import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection, CollectionReference, Query } from '@angular/fire/compat/firestore';
import { uniq } from 'lodash';
import { BehaviorSubject, combineLatest, map, Observable, of, scan, Subscription, switchMap, take } from 'rxjs';
import { User } from 'src/app/models/user';
import { QueryConfig } from 'src/app/models/utilities/QueryConfig';

@Injectable({
  providedIn: 'root'
})
export class PaginationService {
  sub!: Subscription;
  // Source data
  public _done = new BehaviorSubject(false);
  private _loading = new BehaviorSubject(false);
  private _data = new BehaviorSubject<any>([]);

  public query!: QueryConfig;

  // Observable data
  data!: Observable<any>;
  done: Observable<boolean> = this._done.asObservable();
  loading: Observable<boolean> = this._loading.asObservable();


  constructor(private afs: AngularFirestore) { }

  init(path: string, field: string, opts?: any) {
    this._data.next([])
    this._done.next(false)

    this.query = {
      path,
      field,
      limit: 8,
      startAt: Date.now().toString(),
      attribut1: 'category',
      attribut2: 'TempName',
      range: 'price',
      value1: null,
      value2: null,
      rangeValue1: null,
      rangeValue2: null,
      reverse: true,
      prepend: false,
      ...opts
    }
    const first = this.afs.collection<any>(this.query.path, ref => {
      let query: CollectionReference | Query = ref;
      if (this.query.rangeValue1 || this.query.rangeValue2) { query = query.orderBy("price", this.query.reverse ? 'desc' : 'asc') };
      { query = query.orderBy(this.query.field, this.query.reverse ? 'desc' : 'asc').startAfter(this.query.startAt).limit(this.query.limit) };
      if (this.query.value1) { query = query.where(this.query.attribut1, '==', this.query.value1) };
      if (this.query.value2) { query = query.where(this.query.attribut2, '==', this.query.value2) };
      if (this.query.rangeValue1) { query = query.where(this.query.range, '>=', this.query.rangeValue1) };
      if (this.query.rangeValue2) { query = query.where(this.query.range, '<=', this.query.rangeValue2) };
      return query;
    })

    this.mapAndUpdate(first)

    // Create the observable array for consumption in components
    this.data = this._data.asObservable()
  }

  // Retrieves additional data from firestore
  more() {
    const cursor = this.getCursor()
    const more = this.afs.collection(this.query.path, ref => {
      let query: CollectionReference | Query = ref;
      if (this.query.rangeValue1 || this.query.rangeValue2) { query = query.orderBy("price", this.query.reverse ? 'desc' : 'asc') };
      { query = query.orderBy(this.query.field, this.query.reverse ? 'desc' : 'asc').startAfter(cursor).limit(this.query.limit) };
      if (this.query.value1) { query = query.where(this.query.attribut1, '==', this.query.value1) };
      if (this.query.value2) { query = query.where(this.query.attribut2, '==', this.query.value2) };
      if (this.query.rangeValue1) { query = query.where(this.query.range, '>=', this.query.rangeValue1) };
      if (this.query.rangeValue2) { query = query.where(this.query.range, '<=', this.query.rangeValue2) };

      return query;
    })
    this.mapAndUpdate(more)
    this.data = this._data.asObservable().pipe(
      scan((acc, val) => {
        return this.query.prepend ? val.concat(acc) : acc.concat(val)
      }))
  }


  // Determines the doc snapshot to paginate query 
  private getCursor() {
    const current = this._data.value
    if (current.length > 0) {
      return current[current.length - 1].item.createdAt
    }
    return Date.now().toString()
  }



  // Maps the snapshot to usable format the updates source
  private mapAndUpdate(col: AngularFirestoreCollection<any>) {
    if (this._done.value || this._loading.value) { return };
    // loading
    this._loading.next(true)
    // Map snapshot with doc ref (needed for cursor)
     this.sub=col.valueChanges({ idField: 'id' }).pipe(
      switchMap(rooms => {
        if (rooms.length > 0) {
          const users = uniq(rooms.map(bp => bp.OwnerAddress))
          return combineLatest(
            of(rooms),
            combineLatest(
              users.map(OwnerAddress =>
                this.afs.collection<User>('users', ref => ref.where('wallet_address', '==', OwnerAddress)).valueChanges().pipe(
                  map(owners => owners[0])
                )
              )
            )
          )
        } else {
          return of([[], []])
        }
      }),
      map(([rooms, owners]) => {
        let roomsusers = rooms.map(room => {
          return {
            item: room,
            owner: owners.find(a => a.wallet_address === room.OwnerAddress)
          }
        })
        // update source with new values, done loading
        this._data.next(roomsusers)
        this._loading.next(false)
        // no more values, mark done
        if (roomsusers.length < this.query.limit) {
          this._done.next(true)
        }
      }),
      take(1)
    ).subscribe()

  }

}
