import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { arrayUnion } from 'firebase/firestore';
import { map, Observable, take } from 'rxjs';
import {  EventModule } from 'src/app/models/utilities/event/event.module';

@Injectable({
  providedIn: 'root'
})
export class RoomEventsService {

  constructor( private afs: AngularFirestore) { }

  get(eventId:string): Observable<EventModule> {
    return this.afs
      .collection<EventModule>('events')
      .doc(eventId)
      .snapshotChanges()
      .pipe(
        map((doc: { payload: { id: any; data: () => any; }; }) => {
          return { id: doc.payload.id, ...doc.payload.data() };
        })
      );
  }

  async create(uid:string) {
    const data = {
      uid,
      createdAt: Date.now(),
      eventMessage: []
    };
    const docRef = await this.afs.collection('events').add(data);
    return docRef.id;
  }

  async addEventMessage(eventId:string, content:string,name:string,uid:string) {
    const data = {
      uid,
      content,
      name,
      createdAt: Date.now()
    };
    if (uid) {
      const ref = this.afs.collection('events').doc(eventId);
      return ref.update({
        eventMessage: arrayUnion(data)
      });
    }
  }
  
  async deleteMessage(id:string) {
    const ref = this.afs.collection('events').doc(id);    
    const subscration=ref.valueChanges().pipe(take(1)).subscribe(result=>{
    const message=result as EventModule
     message.eventMessage.shift()
     subscration.unsubscribe()
     return ref.update({
      eventMessage: message.eventMessage
    }); 
    })  
  }
  delete(id?: string): Promise<void> {
    return this.afs.collection('events').doc(id).delete();
  }

  
}
