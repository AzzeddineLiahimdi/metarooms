import { Injectable } from '@angular/core';
import { NftItem } from '../../models/nft-item.model';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/compat/firestore';

@Injectable({
  providedIn: 'root'
})
export class MarketItemService {
  private dbPath = '/marketitems';
  marketItemsRef: AngularFirestoreCollection<NftItem>;
  marRef: AngularFirestoreCollection<NftItem>;
  constructor(private db: AngularFirestore) {
    this.marketItemsRef = db.collection(this.dbPath);
    this.marRef=db.collection(this.dbPath, ref => ref.orderBy('tokenId').limitToLast(12));
  }
  getAll(data?:number,next=0): AngularFirestoreCollection<NftItem> {
    if(data == 1){
      return this.db.collection('/marketitems', ref => ref.where("onSale","==", true).orderBy("created").startAfter(next).limit(16))
    }else if(data == 2){
      return this.db.collection('/marketitems', ref => ref.where("onSale","==", false).orderBy("created").startAfter(next).limit(16))
    }else{
      return this.db.collection('/marketitems', ref => ref.orderBy("created").startAfter(next).limit(16))
    
    }
  }
  getLastNFTs(): AngularFirestoreCollection<NftItem>{
    return this.marRef
  }
  async create(nft: NftItem): Promise<string> {
    let id="";
    nft.contract_address = nft.contract_address.toLowerCase()
    nft.created=  Date.now();
  await this.marketItemsRef.add({ ...nft}).then(function(docRef) {
      id=docRef.id
  });
  return id;
  }
  update(id: string, data: any): Promise<void> {
    return this.marketItemsRef.doc(id).update(data);
  }
  delete(id: string): Promise<void> {
    return this.marketItemsRef.doc(id).delete();
  }
  getMarketItemByOwner(creatorAdress:String){
    return this.db.collection('/marketitems', ref => ref.where("creator_address","==", creatorAdress).where("isMinted","==",false)).valueChanges({ idField: 'id' })
  }
  getMarketItemByContract(contract_address:String){
    return this.db.collection('/marketitems', ref => ref.where("contract_address","==", contract_address)).valueChanges({ idField: 'id' })
  }
  getMarketItemOnsale(onsale:String){
    return this.db.collection('/marketitems', ref => ref.where("onSale","==", onsale)).valueChanges({ idField: 'id' })
  }
  getMarketItemByContractAndTokenId(contract_address:String, tokenId:number){
    return this.db.collection('/marketitems', ref => ref.where("contract_address","==", contract_address).where("tokenId","==",tokenId)).valueChanges({ idField: 'id' })
  }
  ChangerState(userKey:string){
    let value={
      isMinted:true,
    }
    return this.db.collection('/marketitems').doc(userKey).update(value);
  }
  ChangeOnSale(userKey?:string,changeValue?:boolean){
    let value={
      onSale:changeValue,
    }
    return this.db.collection('/marketitems').doc(userKey).update(value);
  }
  getMarketItemById(id:string){
    return this.db.collection('/marketitems').doc(id).valueChanges({ idField: 'id' });
   }
}