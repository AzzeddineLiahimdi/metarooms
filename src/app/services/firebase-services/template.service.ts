import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection} from '@angular/fire/compat/firestore';
import { Template } from 'src/app/models/templates';

@Injectable({
  providedIn: 'root'
})
export class TemplateService {
  private dbPath = '/templates';
  tempRef: AngularFirestoreCollection<Template>;


  constructor(private db: AngularFirestore) {
    this.tempRef = db.collection(this.dbPath);
  }
  getAll() {
    return this.db.collection(this.dbPath).valueChanges({ idField: 'id' })
  }
  getAllTemplates() {
    return this.db.collection('/templates').valueChanges({ idField: 'id' })
  }
  async create(temp: Template) {
    return this.tempRef.add({ ...temp})
}
  
  getSpacesByCategory(category?: string) {
    return this.db.collection(this.dbPath, ref => ref.where("category", "==", category)).valueChanges({ idField: 'id' })
  }
  getSpaceById(id:string){
    return this.db.collection(this.dbPath).doc(id).valueChanges({ idField: 'id' });
   }
   getSpaceBySeneName(senename?:string){
    return this.db.collection(this.dbPath, ref => ref.where("seneName", "==", senename)).valueChanges({ idField: 'id' })
   }
}
