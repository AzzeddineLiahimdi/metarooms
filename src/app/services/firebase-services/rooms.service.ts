import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { Room } from 'src/app/models/room';
import { ChatService } from '../chat.service';
import { RoomEventsService } from './room-events.service';

@Injectable({
  providedIn: 'root'
})
export class RoomsService {
  private dbPath = '/rooms';
  data: any[] = [];
  dataC: any[] = [];

  constructor(private db: AngularFirestore, private chatservice: ChatService,private es: RoomEventsService) { }



  async getAll() {
    let i = 0
    return new Promise((resolve, reject) => {
      const sup=this.db
        .collection(this.dbPath)
        .get()
        .subscribe((snapshot: any) => {
          snapshot.docs.forEach((doc: any) => {
            this.db.collection('/users').doc(doc.data().OwnerAddress).get().subscribe((user) => {
              this.data[i] = {
                space: doc.data(),
                owner: user.data(),
                id: doc.id
              }
              i++
            })
            // console.log(doc.data())
          });
          resolve(this.data);
          sup.unsubscribe()
        })
    })
  }
  ListOfMyRooms(address?: string) {
    return this.db.collection(this.dbPath, ref => ref.where("OwnerAddress", "==", address)).valueChanges({ idField: 'id' })
  }
  /*
  async getRoomsByCategory(category: string) {
    this.dataC=[]
    let i = 0
    return new Promise((resolve, reject) => {
      this.db
        .collection(this.dbPath, ref => ref.where("category", "==", category))
        .get()
        .subscribe((snapshot: any) => {
          snapshot.docs.forEach((doc: any) => {
            this.db.collection('/users').doc(doc.data().OwnerAddress).get().subscribe((user) => {
              this.dataC[i] = {
                space: doc.data(),
                owner: user.data(),
                id:doc.id
              }
              i++
            })
            // console.log(doc.data())
          });
          console.log(this.dataC)
          resolve(this.dataC);
        })
    })
  }
  */

  GetRoomById(id?: string) {
    return this.db.collection(this.dbPath).doc(id).valueChanges({ idField: 'id' });
  }
  createRoom(room: Room): any {
    console.log(room)
    return this.db.collection(this.dbPath).add({ ...room });
  }
  delete(id?: string, chatId?: string,eventId?: string): Promise<void> {
    try {
    this.chatservice.delete(chatId)
    this.es.delete(eventId)
    } catch (error) {
      console.log("chat or event message is not deleted")
    } 
    return this.db.collection(this.dbPath).doc(id).delete();
  }
  Update(id: string, jsonDa: string) {
    const value = {
      jsonDa: jsonDa,
    };
    return this.db.collection(this.dbPath).doc(id).update(value);
  }

}
