import { Injectable } from '@angular/core';
import { ethers } from "ethers";
import { environment } from "src/environments/environment";
import lazy from 'src/artifacts/contracts/Marketplace.sol/Marketplace.json';
import {SignHelper} from 'src/app/models/sign-helper';
import detectEthereumProvider from "@metamask/detect-provider";
import { OrderService } from 'src/app/services/firebase-services/order.service';
import { User } from '../models/user';
import { ToastrService } from 'ngx-toastr';

@Injectable({
  providedIn: 'root'
})
export class LazymintingService {
	public actuelUser = new User();

  constructor(private orderservice:OrderService,private toastr: ToastrService    ) { }

  public async lazyMinting(address:string,id:string,voucher:any,standard?:number){
    let nftprice=voucher.price;
    console.log(nftprice,"price")
    voucher.price=ethers.utils.parseUnits(nftprice, 'ether')
    voucher.royalties=1000
    const contract = await LazymintingService.getContractLazy(true)
    let totalprice= (Number(nftprice) * voucher.amountForSale).toFixed(6);
    const price = ethers.utils.parseUnits(totalprice.toString(), 'ether')
    let status=0
    try {
      let transaction = await contract['redeem'](address,voucher.contractAddress,standard,voucher,{ value: price });
      let tx = await transaction.wait()
      status=tx.status
    } catch (error:any) {
      this.toastr.error('Insufficient funds for gas', 'Purchase canceled', {
				timeOut: 7000,
				positionClass: "toast-bottom-right",
			});
    }    
    if(status==1){
      this.orderservice.removeOrder(id)
      return true
    }else{
      return false
    }
    
  }
  private static async getContractLazy(bySigner = false) {
    const provider = await LazymintingService.getWebProvider()
    const signer = provider.getSigner()

    return new ethers.Contract(
      environment.NFTMARKET_CONTRACT_ADDRESS,
      lazy.abi,
      bySigner ? signer : provider,
    )
  }
  private static async getWebProvider(requestAccounts = true) {
    const provider: any = await detectEthereumProvider()
    if (requestAccounts) {
      await provider.request({ method: 'eth_requestAccounts' })
    }
    return new ethers.providers.Web3Provider(provider)
  }

  public async AddSignature(metadata:any,price:Number,amount:number,totalamount:number,royalties:number,tokenId:any,tokenOwner:string,contract:string,image:string,title:string,Category:string):Promise<boolean>{
    const contracto = await LazymintingService.getContractLazy(true)
    let nonce = await contracto['getCurrentSalesOrderNonce'](tokenOwner);
    //1000
    var voucher=await SignHelper.getSing(contract,tokenOwner,137,tokenId,metadata,price.toString(),totalamount,amount,1000,Number(nonce));
    try {
    await this.orderservice.createOrder(price,amount,voucher.tokenOwner,voucher.contractAddress,voucher.uri,voucher.royalties,voucher.tokenId,voucher.signature,image,title,Number(nonce),Category);
    return true  
    } catch (error) {
      return false  
    }  
  }

}
