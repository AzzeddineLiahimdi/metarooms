import { Injectable } from '@angular/core';
import { getNftMetadata, initializeAlchemy, Network, getNftsForCollectionIterator, getNftsForOwnerIterator, getOwnersForNft, Nft, getNftsForOwner, OwnedNft } from '@alch/alchemy-sdk';
import { environment } from 'src/environments/environment';
import { NftData, Werabeals } from '../models/werabeals';
import { OrderService } from './firebase-services/order.service';



@Injectable({
  providedIn: 'root'
})
export class AlchemyService {
  public config = {
    apiKey: environment.API_KEY,
    network: Network.MATIC_MAINNET,
    maxRetries: 10
  };
  public alchemy: any;

  constructor(private orderService: OrderService) {
    this.alchemy = initializeAlchemy(this.config);
  }

  public async getDCnftsByAddress(OwnerAddress: string) {
    var DCWL = new Array();
    var DCemote = new Array();
    var emotes = ["dance", "poses", "fun", "greetings", "", "Horror", "Miscellaneous", "Stunt", "Reactions"]
    let data;

    for await (const nft of getNftsForOwnerIterator(this.alchemy, OwnerAddress, {
    })) {
      //we see if the nft metadata comes from the decentraland api
      if (nft.tokenUri?.raw?.includes("decentraland.org") && nft.rawMetadata?.attributes) {
        const nftD = new Werabeals(nft.tokenId, nft.contract.address, nft.rawMetadata['id'].split(":")[5]);
        data = await this.orderService.getOrderByContract(nft.contract.address, nft.tokenId)
        let nftdata = {
          nft: nftD,
          isListed: data.isListed,
          price: data.price
        }
        if (emotes.includes(nft.rawMetadata?.attributes[1]?.['value'])) {
          console.log(nft.tokenId, nft.contract.address,nft.rawMetadata['id'].split(":")[5],"emotes test")
          DCemote.push(new NftData(nftdata))
        } else {
          DCWL.push(new NftData(nftdata))
        }
      }
    }
    return ([DCemote, DCWL])
  }

  public async getNftsByContractAndUser(OwnerAddress: string, contract: string) {
    var DCWL = new Array();
    let i = 0;
    for await (const nft of getNftsForOwnerIterator(this.alchemy, OwnerAddress, { contractAddresses: [contract] })) {
      await getOwnersForNft(this.alchemy, nft).then(response =>
        response.owners.forEach(owner => {
          if (owner == OwnerAddress) {
            DCWL[i] = nft
            i++
          }
        })
      );
    }
    return (DCWL)
  }
  public async NftInCollection(contract: string) {

    for await (const nft of getNftsForCollectionIterator(this.alchemy, contract)) {
      // @ts-ignore
      console.log(nft.tokenId, nft.rawMetadata?.id[nft.rawMetadata?.id?.length - 1])
    }
  }
  public async getNftsForOwnerIterator(OwnerAddress: string) {
    var DCWL = new Array();
    let i = 0;
    for await (const nft of getNftsForOwnerIterator(this.alchemy, OwnerAddress)) {
      if (nft.rawMetadata?.image?.includes("decentraland.org")) {
        DCWL[i] = nft
        i++
      }
    }
    return (DCWL)
  }
  public async getNftsForOwner(address: string, pageKey?: string): Promise<[OwnedNft[], string | undefined]> {
    let nfts = await getNftsForOwner(this.alchemy, address, { pageKey: pageKey })
    let newArray = nfts.ownedNfts.filter(nft => nft.rawMetadata?.image?.includes("decentraland.org"));
    return ([newArray, nfts.pageKey])
  }

  public async getMetadata(contract: string, tokenid: any): Promise<Nft> {
    let asset = await getNftMetadata(this.alchemy,
      contract,
      tokenid
    ) as Nft
    return asset
  }

  public async getOwnersForNft(asset: any) {
    let owners = await getOwnersForNft(this.alchemy, asset)
    return owners
  }
  public async getNftMetadata(contract: string, tokenId: string) {
    return await getNftMetadata(this.alchemy, contract, tokenId)
  }
}


