import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection, CollectionReference, Query } from '@angular/fire/compat/firestore';
import { uniq } from 'lodash';
import { BehaviorSubject, combineLatest, map, Observable, of, scan, switchMap, take, tap } from 'rxjs';
import { User } from '../models/user';
interface QueryConfig {
  path: string, //  path to collection
  field: string, // field to orderBy
  limit: number, // limit per query
  startAt: any,
  attribut1: string,
  attribut2: string
  value1: string,
  value2: string,
  reverse: boolean, // reverse order?
  prepend: boolean // prepend to source?
}


@Injectable({
  providedIn: 'root'
})
export class TestService {

  // Source data
  private _done = new BehaviorSubject(false);
  private _loading = new BehaviorSubject(false);
  public _data = new BehaviorSubject<any>([]);

  public query!: QueryConfig;

  // Observable data
  data!: Observable<any>;
  done: Observable<boolean> = this._done.asObservable();
  loading: Observable<boolean> = this._loading.asObservable();


  constructor(private afs: AngularFirestore) { }

  init(path: string, field: string, opts?: any) {
    this._data.next([])

    this.query = {
      path,
      field,
      limit: 2,
      startAt: Date.now().toString(),
      attribut1: 'category',
      attribut2: 'TempName',
      value1: null,
      value2: null,
      reverse: true,
      prepend: false,
      ...opts
    }

    const first = this.afs.collection<any>(this.query.path, ref => {
      let query: CollectionReference | Query = ref;
      { query = query.orderBy(this.query.field, this.query.reverse ? 'desc' : 'asc').startAfter(this.query.startAt).limit(this.query.limit) };
      if (this.query.value1) { query = query.where(this.query.attribut1, '==', this.query.value1) };
      if (this.query.value2) { query = query.where(this.query.attribut2, '==', this.query.value2) };
      return query;
    })

    this.mapAndUpdate(first)

    // Create the observable array for consumption in components
    this.data = this._data.asObservable().pipe(
      scan((acc, val) => {
        return this.query.prepend ? val.concat(acc) : acc.concat(val)
      }))
  }

  // Retrieves additional data from firestore
  more() {
    const cursor = this.getCursor()

    const more = this.afs.collection(this.query.path, ref => {
      let query: CollectionReference | Query = ref;
      { query = query.orderBy(this.query.field, this.query.reverse ? 'desc' : 'asc').startAfter(cursor).limit(this.query.limit) };
      if (this.query.value1) { query = query.where(this.query.attribut1, '==', this.query.value1) };
      if (this.query.value2) { query = query.where(this.query.attribut2, '==', this.query.value2) };
      return query;
    })
    this.mapAndUpdate(more)
  }


  // Determines the doc snapshot to paginate query 
  private getCursor() {
    const current = this._data.value
    if (current.length > 0) {
      return current[current.length - 1].space.createdAt
    }
    return Date.now().toString()
  }



  // Maps the snapshot to usable format the updates source
  private mapAndUpdate(col: AngularFirestoreCollection<any>) {

    if (this._done.value || this._loading.value) { return };

    // loading
    this._loading.next(true)

    // Map snapshot with doc ref (needed for cursor)
    return col.valueChanges({ idField: 'id' }).pipe(
      switchMap(rooms => {
        if (rooms.length > 0) {
          const users = uniq(rooms.map(bp => bp.OwnerAddress))
          return combineLatest(
            of(rooms),
            combineLatest(
              users.map(OwnerAddress =>
                this.afs.collection<User>('users', ref => ref.where('wallet_address', '==', OwnerAddress)).valueChanges().pipe(
                  map(owners => owners[0])
                )
              )
            )
          )
        } else {
          return of([[], []])
        }
      }),
      map(([rooms, owners]) => {
        console.log(rooms, 'roooms')
        let roomsusers = rooms.map(room => {
          return {
            space: room,
            owner: owners.find(a => a.wallet_address === room.OwnerAddress)
          }
        })
        // update source with new values, done loading
        this._data.next(roomsusers)
        console.log(this._data.value)
        this._loading.next(false)
        // no more values, mark done
        if (!roomsusers.length) {
          this._done.next(true)
        }

      }),
      take(1)
    ).subscribe()

  }

}
