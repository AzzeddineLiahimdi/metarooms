import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { DclNFT } from 'src/app/models/decl/dclnft';
import { GetDataService } from 'src/app/services/decentraland-services/get-data.service';
import Swal from 'sweetalert2';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  public trendings$!: Observable<DclNFT[]>;
  public newest$!: Observable<DclNFT[]>;
  public Creators$!: Observable<any[]>;
  public addidasItems:any[]=[];
  public collections!: any[];
  public collection$!:Observable<any[]>
  public images: any;
  public responsiveOptions: any;

  constructor(private dcldata: GetDataService) {
    this.responsiveOptions = [
      {
        breakpoint: '1024px',
        numVisible: 3,
        numScroll: 3
      },
      {
        breakpoint: '768px',
        numVisible: 2,
        numScroll: 2
      },
      {
        breakpoint: '560px',
        numVisible: 1,
        numScroll: 1
      }
    ];
  }

  async ngOnInit(): Promise<void> {
    this.dcldata.getNewestsCollection()
    this.collection$=this.dcldata.getCollections()
    this.collection$.subscribe(data => {
      console.log(data)
    })
    this.trendings$ = this.dcldata.getTrendings()
    this.newest$ = this.dcldata.getNewests()
    this.Creators$ = this.dcldata.getPopularCreators()
    this.dcldata.getAdidasItems("ETHEREUM","0xba0c9cf4da821dba98407cc4f9c11f6c7a5f9bbc").subscribe(async data => {
      await data.items.forEach((item: any) => {
        this.dcldata.getOrders("ETHEREUM","0xba0c9cf4da821dba98407cc4f9c11f6c7a5f9bbc",item.tokenId).subscribe(order => {
          this.addidasItems.push({ ...item, orders: order })
        })

      });
    })
 
  }

  afficherVideo(url: string, name: string, price: string) {
    Swal.fire({
      title: name,
      text: name,
      showCancelButton: true,
      confirmButtonText: price = "NO" ? "Place a bid" : "Buy now for " + price + " ETH",
      html: '<video loop muted autoplay oncanplay="this.play()" onloadedmetadata="this.muted = true" width="400" controls><source src="' + url + '" type="video/mp4"><source src="mov_bbb.ogg" type="video/ogg">Your browser does not support HTML video.</video>',
    })
  }


}
