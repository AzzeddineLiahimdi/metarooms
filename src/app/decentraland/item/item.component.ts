import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { ActivatedRoute } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { Observable, Subscription } from 'rxjs';
import { DclNFT } from 'src/app/models/decl/dclnft';
import { User } from 'src/app/models/user';
import { AuthService } from 'src/app/services/auth.service';
import { ContractsService } from 'src/app/services/contracts.service';
import { GetDataService } from 'src/app/services/decentraland-services/get-data.service';
import { UserService } from 'src/app/services/firebase-services/user.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.css']
})
export class ItemComponent implements OnInit {
  public item$!: Observable<DclNFT>;
  public collection!: string
  public itemId!: number
  public actuelUser = new User();
  address = "";
  tmp: any;
  userrSup!: Subscription;
  loginUser: boolean = false;
  addressUserView: boolean = false
  mana: any

  constructor(private auth: AuthService, private cdr: ChangeDetectorRef, private db: UserService, private DclContract: ContractsService,
    private dcldata: GetDataService, private router: ActivatedRoute, private sanitizer: DomSanitizer,	private spinnerService: NgxSpinnerService,
    ) { }

  ngOnInit(): void {
    this.spinnerService.show()
    this.auth.loginUser.subscribe((res: boolean) => {
      this.loginUser = res;
      (!this.loginUser) ? this.addressUserView = false : this.addressUserView = true;
      this.cdr.detectChanges();
    });
    this.auth.addressUser.subscribe((res: string) => {
      this.address = res;
      this.dcldata.GetMANAbalance(this.address).subscribe(data => {
        this.mana = data
        //console.log(this.mana.normalBalance)
      })
      //this.getUser()
      this.cdr.detectChanges();
    })
    this.router.params.subscribe(async params => {
      this.collection = await params['collection'];
      this.itemId = await params['itemId'];
      this.item$ = this.dcldata.getItem(this.collection, this.itemId)
      this.item$.subscribe(data=>{
        this.spinnerService.hide()
      })
    })

  }

  AanimationURL() {
    return this.sanitizer.bypassSecurityTrustResourceUrl("https://wearable-preview.decentraland.org/?contract=" + this.collection + "&item=" + this.itemId + "&profile=default&emote=fashion-2&background=D6E3E9");
  }

  async Buy(price: number, name: string) {
    if (this.mana.normalBalance >= price) {
      Swal.fire({
        title: 'Buy Wearable',
        text: "You are about to buy Quantum Fighter - Jacket & Blade for " + price + " MANA",
        showCancelButton: true,
        confirmButtonText: 'Buy',
      }).then(async (result) => {
        /* Read more about isConfirmed, isDenied below */
        if (result.isConfirmed) {
          try {
            Swal.fire({
              showConfirmButton: false,
              title: 'Buy Wearable',
              text: "You are about to buy " + name + " " + price + " MANA",
              allowOutsideClick: false,
              didOpen: () => {
                Swal.showLoading(Swal.getDenyButton())
              }
            })
            const status = await this.DclContract.Buy(this.collection, Number(this.itemId), Number(price), this.address)
            console.log(status, "status")
            if (!status) {
              this.cancelMessage()
            } else {
              Swal.fire({
                position: 'top-end',
                icon: 'success',
                title: 'purchase completed ',
                showConfirmButton: false,
                timer: 1500
              })
            }
          } catch (error) {
            this.cancelMessage()
          }
        } else if (result.isDismissed) {
          this.cancelMessage()
        }
      })
    } else {
      Swal.fire({
        icon: 'error',
        title: "You don't have enough MANA to mint " + name + " " + price + " mana",
        showConfirmButton: false,
        timer: 2500
      })
    }
  }

  cancelMessage() {
    Swal.fire({
      position: 'top-end',
      icon: 'warning',
      title: 'Purchase canceled',
      showConfirmButton: false,
      timer: 1500
    })
  }

}
