import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { ActivatedRoute } from '@angular/router';
import { createRaribleSdk } from '@rarible/protocol-ethereum-sdk';
import { Web3Ethereum } from '@rarible/web3-ethereum';
import { NgxSpinnerService } from 'ngx-spinner';
import { Observable, Subscription } from 'rxjs';
import { User } from 'src/app/models/user';
import { AuthService } from 'src/app/services/auth.service';
import { GetDataService } from 'src/app/services/decentraland-services/get-data.service';
import { UserService } from 'src/app/services/firebase-services/user.service';
import Swal from 'sweetalert2';
import Web3 from 'web3';
declare let window: any

@Component({
  selector: 'app-items',
  templateUrl: './items.component.html',
  styleUrls: ['./items.component.css']
})
export class ItemsComponent implements OnInit {
  public item$!: Observable<any>;
  public orders$!: Observable<any>;
  public collection$!: Observable<any>;

  public collection = ""
  public tokenId = 0
  public actuelUser = new User();
  public network!: string;
  address = "";
  tmp: any;
  userrSup!: Subscription;
  loginUser: boolean = false;
  addressUserView: boolean = false
  mana: any
  float2int(value: string) {
    const cmp = Number(value)
    return cmp | 0;
  }
  constructor(private auth: AuthService, private cdr: ChangeDetectorRef, private db: UserService,
    private dcldata: GetDataService, private router: ActivatedRoute, private sanitizer: DomSanitizer,	private spinnerService: NgxSpinnerService) { }

  ngOnInit(): void {
    this.spinnerService.show()
    this.auth.loginUser.subscribe((res: boolean) => {
      this.loginUser = res;
      (!this.loginUser) ? this.addressUserView = false : this.addressUserView = true;
      this.cdr.detectChanges();
    });
    this.auth.addressUser.subscribe((res: string) => {
      this.address = res;
      this.cdr.detectChanges();
    })
    this.router.params.subscribe(async params => {
      this.collection = await params['collection'];
      this.tokenId = await params['tokenId'];
      this.network = await params['network'];
      this.item$ = this.dcldata.getItembyId(this.network + ":" + this.collection + ":" + this.tokenId)
      this.item$.subscribe(item => {
        console.log(item)
        this.orders$ = this.dcldata.getOrders(this.network, this.collection, item.tokenId)
        this.collection$=this.dcldata.getcollection(this.collection,this.network)
        this.collection$.subscribe(data=>{
          console.log(data)
          this.spinnerService.hide()
        })
      })
    })
  }
  async buy(id: string) {
    if (this.loginUser == false) {
      Swal.fire({
        imageUrl: '../../../assets/img/metamask.gif',
        imageWidth: 80,
        imageHeight: 70,
        title: "<div style='margin: 0 0 -5px !important;padding: 0 !important;'>You mast connect your wallet</div>",
        confirmButtonText: '<div style="width: 15em;">Connect wallet</div>',
        confirmButtonColor: '#3085d6',
        showCancelButton: true,
        cancelButtonText: "<div style='width: 15em;'>No thanks</div>",
        allowOutsideClick: false,
        width: 600,
      }).then(async (result) => {
        if (result.isConfirmed) {
          await this.auth.connect()
        }
      })
    } else {
      const chainId=this.network=="ETHEREUM"?"0x1":"0x89";
      const blockchain=this.network=="ETHEREUM"?"Ethereum":"polygon";
      const status = await this.auth.switchNetwork(chainId, blockchain)
      if (status == true) {
        Swal.fire({
          showConfirmButton: false,
          title: 'Confirm transaction to purchase this item',
          allowOutsideClick: false,
          didOpen: () => {
            Swal.showLoading(Swal.getDenyButton())
          }
        })

        if (!window.ethereum) return;
        const { ethereum } = window;
        if (ethereum && ethereum.isMetaMask) {
          const web3 = new Web3(window.ethereum);
          const wallet = new Web3Ethereum({ web3 });
          // configure raribleSdk
          const netowrk=this.network=="ETHEREUM"?"mainnet":"polygon";
          const raribleSdk = createRaribleSdk(wallet, netowrk);
          const order = await raribleSdk.apis.order.getOrderByHash({ hash: id.replace('ETHEREUM:', '').replace('POLYGON:', '') })
          console.log(order)
          try {
            switch (order.type) {
              case "RARIBLE_V1":
                await raribleSdk.order.buy({ order, amount: parseInt("1"), originFee: 0 })
                Swal.close()
                break;
              case "RARIBLE_V2":
                await raribleSdk.order.buy({ order,amount: parseInt("1") })
                Swal.close()
                break;
              case "SEAPORT_V1":
                //await raribleSdk.order.buy({ order, amount: parseInt("1") })
                Swal.close()
                Swal.fire({
                  icon: 'error',
                  title: "This order type is not yet supported, you can buy it from the source",
                  confirmButtonText: '<a class="text-decoration-none link-dark" target="_blank" href="https://rarible.com/token/'+this.collection+':'+this.tokenId+'?tab=overview">Buy from source</a>',
                  confirmButtonColor: '#3085d6',
                  showCancelButton: true,
                  cancelButtonText: "No thanks",
                })
                break;
              case "LOOKSRARE":
                  await raribleSdk.order.buy({ order, amount: parseInt("1") })
                Swal.close()
                break;
              default:
                Swal.close()
                throw new Error(`Unsupported order : ${JSON.stringify(order)}`)
            }
          } catch (error) {
            console.log(error)
            Swal.close()
          }

        }
      }
    }
  }

}
