import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MenuItem } from 'primeng/api';
import { Subscription } from 'rxjs';
import { ParamsDcl } from 'src/app/models/decl/params';
import { AuthService } from 'src/app/services/auth.service';
import { GetDataService } from 'src/app/services/decentraland-services/get-data.service';
import Swal from 'sweetalert2';
import Web3 from 'web3';
declare let window: any
import { Web3Ethereum } from "@rarible/web3-ethereum";
import { createRaribleSdk } from "@rarible/protocol-ethereum-sdk"

@Component({
  selector: 'app-collection',
  templateUrl: './collection.component.html',
  styleUrls: ['./collection.component.css']
})
export class CollectionComponent implements OnInit, OnDestroy {
  public items: any[] = [];
  public collection: string = "0xba0c9cf4da821dba98407cc4f9c11f6c7a5f9bbc"
  public network!: string
  public sets = new ParamsDcl()
  public def = new ParamsDcl()
  public clear = false
  public browse = false
  public more = false
  sub!: Subscription;
  public loading = true
  public category!: MenuItem[]
  public sex!: MenuItem[]
  public rarity!: MenuItem[]
  public continuation!: string
  constructor(private auth: AuthService, private dcldata: GetDataService, private router: ActivatedRoute) {
    this.category = [
      {
        label: 'All', icon: 'pi pi-refresh', command: () => {
          this.sets.category = "All"
          this.Browse()
        },
      },
      {
        label: 'Head', icon: 'pi pi-refresh', command: () => {
          this.sets.category = "isWearableHead"
          this.Browse()
        },
      },
      {
        label: 'Upper body', icon: 'pi pi-refresh', command: () => {
          this.sets.category = "upper_body"
          this.Browse()
        },
      },
      {
        label: 'Lower Body', icon: 'pi pi-refresh', command: () => {
          this.sets.category = "lower_body"
          this.Browse()
        },
      },
      {
        label: 'Feet', icon: 'pi pi-refresh', command: () => {
          this.sets.category = "feet"
          this.Browse()
        },
      },
      {
        label: 'Accessories', icon: 'pi pi-refresh', command: () => {
          this.sets.category = "isWearableAccessory"
          this.Browse()
        },
      },
      {
        label: 'Skins', icon: 'pi pi-refresh', command: () => {
          this.sets.category = "skin"
          this.Browse()
        },
      }
    ];
    this.sex = [
      {
        label: 'All', icon: 'pi pi-refresh', command: () => {
          this.sets.sex = "All"
          this.Browse()
        },
      },
      {
        label: 'Male', icon: 'pi pi-refresh', command: () => {
          this.sets.sex = "male"
          this.Browse()
        },
      },
      {
        label: 'Female', icon: 'pi pi-refresh', command: () => {
          this.sets.category = "female"
          this.Browse()
        },
      }
    ];
    this.rarity = [
      {
        label: 'All', icon: 'pi pi-refresh', command: () => {
          this.sets.rarity = "All"
          this.Browse()
        },
      },
      {
        label: 'Epic', icon: 'pi pi-refresh', command: () => {
          this.sets.rarity = "epic"
          this.Browse()
        },
      },
      {
        label: 'Mythic', icon: 'pi pi-refresh', command: () => {
          this.sets.rarity = "mythic"
          this.Browse()
        },
      },
      {
        label: 'Legendary', icon: 'pi pi-refresh', command: () => {
          this.sets.rarity = "legendary"
          this.Browse()
        },
      },
      {
        label: 'Unique', icon: 'pi pi-refresh', command: () => {
          this.sets.rarity = "unique"
          this.Browse()
        },
      },
      {
        label: 'Rare', icon: 'pi pi-refresh', command: () => {
          this.sets.rarity = "rare"
          this.Browse()
        },
      }
    ];
  }

  async ngOnInit(): Promise<void> {
    this.router.params.subscribe(async params => {
      this.collection = await params['collection'];
      this.network = await params['network'];
      console.log(this.network, this.collection)
      if (this.collection) {
        if (this.network == undefined) {
          this.dcldata.getitemsBycollection(this.collection).subscribe(data => {
            this.items = data
            console.log(this.items)
            this.loading = false
          })
          this.browse = false
        } else {
          this.dcldata.getRaribleItemsByCollection(this.collection, this.network).subscribe(data => {
            this.continuation = data.continuation
            data.items.forEach((item: any) => {
              this.dcldata.getOrders(this.network,this.collection,item.tokenId).subscribe(order => {
                this.items.push({ ...item, orders: order })
              })
            });
            this.loading = false
            console.log(this.items)
          })
          this.browse = true
          this.more = true
        }
      } else {
        this.browse = true
        this.Browse(true)
      }
    })
  }
  Browse(def = false, loadMore = false) {
    this.loading = true
    if (this.network == undefined) {
      if (this.sets != this.def) {
        this.clear = true
      }
      if (def) {
        this.sets = new ParamsDcl()
        this.clear = false
      }
      this.sub = this.dcldata.Browse(def ? this.def : this.sets, loadMore ? this.items.length : 0).subscribe(data => {
        if (loadMore) {
          this.items = this.items.concat(data)
          this.more = data.length == 24
        } else {
          this.more = data.length == 24
          this.items = data
        }
        this.loading = false
      })
    } else {
      this.dcldata.getAdidasItems(this.network,this.collection,loadMore ? this.continuation : '').subscribe(data => {
        this.continuation = data.continuation
        data.items.forEach((item: any) => {
          this.dcldata.getOrders(this.network,this.collection,item.tokenId).subscribe(order => {
            this.items.push({ ...item, orders: order })
          })
        });
        this.loading = false
      })
      this.browse = true
      this.more = true
    }
  }
  ngOnDestroy() {
    if (this.sub) {
      this.sub.unsubscribe()
    }
  }

  afficherVideo(url: string, name: string, price: string, id: string) {
    Swal.fire({
      title: name,
      text: name,
      showCancelButton: true,
      confirmButtonText: price == "NO" ? "Place a bid" : "Buy now for " + price + " ETH",
      html: '<img src="' + url + '"></img>',
    }).then(async (result) => {
      if (result.isConfirmed) {
        //this.auth.switchChain('0x1')
        if (!window.ethereum) return;
        const { ethereum } = window;
        if (ethereum && ethereum.isMetaMask) {
          console.log("Ethereum successfully detected!");
          const web3 = new Web3(window.ethereum);
          const wallet = new Web3Ethereum({ web3 });
          // configure raribleSdk
          const raribleSdk = createRaribleSdk(wallet, "mainnet");
          console.log(raribleSdk, id.replace('ETHEREUM:', ''),id)
          const order = await raribleSdk.apis.order.getOrderByHash({ hash: id.replace('ETHEREUM:','') })
          console.log(order, "order", order.type)

          switch (order.type) {
            case "RARIBLE_V1":
              await raribleSdk.order.buy({ order, amount: parseInt("1"), originFee: 0 })
              break;
            case "RARIBLE_V2":
              await raribleSdk.order.buy({ order, payouts: [], originFees: [], amount: 1 })
              //const buy = await raribleSdk.order.buy({})
              break;
            case "OPEN_SEA_V1":
              await raribleSdk.order.buy({ order, amount: parseInt("1") })
              break;
            case "SEAPORT_V1":
              await raribleSdk.order.buy({ order, amount: parseInt("1"), originFee: 0 })
              break;
            default:
              throw new Error(`Unsupported order : ${JSON.stringify(order)}`)
          }
        }
      }
    })
  }

}


