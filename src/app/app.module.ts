import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import {MatProgressBarModule} from '@angular/material/progress-bar';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { ReactiveFormsModule } from "@angular/forms";
import { FormsModule } from '@angular/forms';
import { NgxSpinnerModule } from 'ngx-spinner';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule } from "@angular/common/http";
import { MatFormFieldModule } from "@angular/material/form-field";
import { MatIconModule } from "@angular/material/icon";
import { MatInputModule } from "@angular/material/input";
import { TestComponent } from './components/test/test.component';
import { HomePageComponent } from './components/home-page/home-page.component';

import { AngularFireModule } from '@angular/fire/compat';
import { AngularFireAuthModule } from '@angular/fire/compat/auth';
import { AngularFireStorageModule } from '@angular/fire/compat/storage';
import { AngularFirestoreModule } from '@angular/fire/compat/firestore';
import { AngularFireDatabaseModule } from '@angular/fire/compat/database';
import { environment } from '../environments/environment';
import { TemplatesComponent } from './components/templates/templates.component';
import { MyroomsComponent } from './components/myrooms/myrooms.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { SpinnerComponent } from './components/spinner/spinner.component';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import {MatSelectModule} from '@angular/material/select';
import { MarketComponent } from './components/market/market.component';
import { AssetComponent } from './components/asset/asset.component';
import { CreateTemplateComponent } from './components/create-template/create-template.component';
import { AssetsComponent } from './components/assets/assets.component';
import { ToastrModule } from 'ngx-toastr';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { UnityComponent } from './components/unity/unity.component';
import { PagenotfoundComponent } from './components/pagenotfound/pagenotfound.component';
import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';
import { ChatComponent } from './components/utilities/chat/chat.component';
import { EventComponent } from './components/utilities/event/event.component';
import { HomeComponent } from './decentraland/home/home.component';
import {CarouselModule} from 'primeng/carousel';
import { ButtonModule } from 'primeng/button';
import {TabViewModule} from 'primeng/tabview';
import { CollectionComponent } from './decentraland/collection/collection.component';
import { ItemComponent } from './decentraland/item/item.component';
import {SplitButtonModule} from 'primeng/splitbutton';
import { TagModule } from 'primeng/tag';
import { ItemsComponent } from './decentraland/items/items.component';


@NgModule({
  declarations: [
    AppComponent,
    TestComponent,
    HomePageComponent,
       TemplatesComponent,
       MyroomsComponent,
       NavbarComponent,
       SpinnerComponent,
       MarketComponent,
       AssetComponent,
       CreateTemplateComponent,
       AssetsComponent,
       UnityComponent,
       PagenotfoundComponent,
       ChatComponent,
       EventComponent,
       HomeComponent,
       CollectionComponent,
       ItemComponent,
       ItemsComponent,
  ],
  imports: [
    SplitButtonModule,
    TabViewModule,
    ButtonModule,
    CarouselModule,
    NgxSkeletonLoaderModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot(),
    MatProgressBarModule,
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    NoopAnimationsModule,
    MatIconModule,
    NgxSpinnerModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireAuthModule,
    AngularFirestoreModule,
    AngularFireStorageModule,
    AngularFireDatabaseModule,
    FormsModule,
    MatSelectModule,
    TagModule,
    MatFormFieldModule,
    MatInputModule,
    MatSlideToggleModule
  ],
  providers: [
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
