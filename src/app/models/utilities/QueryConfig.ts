
// Options to reproduce firestore queries consistently
export interface QueryConfig {
  path: string, //  path to collection
  field: string, // field to orderBy
  limit: number, // limit per query
  startAt: any,
  attribut1: string,
  attribut2: string,
  range:string,
  value1: string,
  value2: string,
  rangeValue1:number,
  rangeValue2:number,
  reverse: boolean, // reverse order?
  prepend: boolean // prepend to source?
}
