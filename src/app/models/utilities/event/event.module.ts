import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';



@NgModule({
  declarations: [],
  imports: [
    CommonModule
  ]
})
export class EventModule {
  id!: string
  uid!: string
  createdAt!: number
  eventMessage!: EventMessage[]
}

export interface EventMessage {
  createdAt: number
  name:string
  uid: string
  content: string
}
