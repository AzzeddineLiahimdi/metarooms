import { ethers } from "ethers";
import { environment } from "src/environments/environment";
declare let window: any;
const SIGNING_DOMAIN_NAME = "Omnispaces"
const SIGNING_DOMAIN_VERSION = "1"
export class SignHelper {
    contractAddress: string;
    chainId: number;
    signer: any;
    _domain: any;
    
    constructor(contractAddress:string,chainId:number,signer:any){
        this.contractAddress=contractAddress;
        this.chainId=chainId;
        this.signer=signer;
        this._domain=null;
    }
    async _signingDomain(){
        if(this._domain !=null){
            return this._domain
        }
        const chainId=await this.chainId
        this._domain={
            name:SIGNING_DOMAIN_NAME,
            version:SIGNING_DOMAIN_VERSION,
            verifyingContract:this.contractAddress,
            chainId
        }
        return this._domain
    }
    async createSignature(tokenId:number,contractAddress:string,tokenOwner:string,uri:string, price = ethers.utils.parseUnits('0.005', 'ether'),amount=1,amountForSale=1,royalties=10,nonce=0){
        const obj={ contractAddress,tokenId,tokenOwner,price,amount,amountForSale,royalties,uri,nonce}
        const domain=await this._signingDomain()
        const types={
            NFTVoucher: [
                {name: "contractAddress", type: "address"},
                {name: "tokenId", type: "uint256"},
                {name: "tokenOwner", type: "address"},
                {name: "price", type: "uint256"},
                {name: "amount", type: "uint256"},
                {name: "amountForSale", type: "uint256"},
                {name: "royalties", type: "uint256"},
                {name: "uri", type: "string"},
                {name: "nonce", type: "uint256"},  
              ]
        }
        const signature=await this.signer._signTypedData(domain,types,obj)
        return {...obj,signature,}
    }
    static async getSing(contractAddress:string,owner:string,chainId:number,tokenId:number,name:any,price:string,amount:number,amountForSale:number,royalties:number,nonce:number){
        
        var provider=new ethers.providers.Web3Provider(window.ethereum)
        await provider.send("eth_requestAccounts",[]);
        var signer=provider.getSigner()
        await signer.getAddress()
        console.log(tokenId,signer)
        var lm=new SignHelper(environment.NFTMARKET_CONTRACT_ADDRESS,chainId,signer)
        var voucher= await lm.createSignature(tokenId,contractAddress,owner,name,ethers.utils.parseUnits(price, 'ether'),amount,amountForSale,royalties,nonce)
        return voucher
    }

}
