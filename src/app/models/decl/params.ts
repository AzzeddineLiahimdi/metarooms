export class ParamsDcl {
    color!: string;
    category: string
    creator!: string
    rarity: string
    sex: string
    pricefrom!:string
    priceTo!:string
    constructor(){
      this.sex="All";
      this.category="All";
      this.rarity="All";
    }
  }