export interface DclNFT {
  id: string
  name: string
  thumbnail: string
  url: string
  category: string
  contractAddress: string
  itemId: string
  beneficiary: string
  rarity: string
  price: number
  available: number
  isOnSale: boolean
  creator: string
  data: Data
  network: string
  chainId: number
  createdAt: number
  updatedAt: number
  reviewedAt: number
  soldAt: number
}

export interface Data {
  wearable: Wearable
}

export interface Wearable {
  description: string
  category: string
  bodyShapes: string[]
  rarity: string
  isSmart: boolean
}