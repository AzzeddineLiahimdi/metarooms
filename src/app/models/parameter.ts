import { NftData } from "./werabeals";

export class EventData {
    public eventName!: string;
    public parameters!: any;
    private constructor2(name: string, parameters: any) {
        this.eventName = name
        this.parameters = parameters
    };
    private constructor1(data: EventData) {
        this.eventName = data.eventName
        if (data.eventName = "Order") {
            this.parameters = new OrderData(JSON.parse(data.parameters))
        } else {
            this.parameters = new MarketOperationData(JSON.parse(data.parameters))
        }
    };
    constructor(...attributes: any[]) {
        if (attributes.length === 1) {
            this.constructor1(attributes[0])
            return;
        }
        if (attributes.length === 2) {
            this.constructor2(attributes[0], attributes[1])
            return;
        }
    }
}


export class AvatarData {
    public avatarName!: string;
    public avatarType!: string;
    public avatarUrl!: string;

    constructor(name: string, type: string, url: string) {
        this.avatarName = name,
            this.avatarType = type,
            this.avatarUrl = url
    }
}
export class GameData {
    public mode!: string;
    public sceneName!: string;
    public template!: string;
    public roomName!: string;
    public roomOwner!: string;
    public roomData!: string;
    constructor(mode: string, scene: string, room: any) {
        this.mode = mode,
            this.sceneName = scene,
            this.template = scene,
            this.roomName = room.roomName,
            this.roomOwner = room.OwnerAddress,
            this.roomData = room.jsonDa
    }
}
export class WalletData {
    public walletAddress!: string;
    public Wearables: NftData[] = [];
    public arts: NftData[] = [];
    public artsVideo: NftData[] = [];
    public arts3D: NftData[] = [];
    public emotes: NftData[] = [];
    
    constructor(address: string, Wearables: NftData[],emotes: NftData[], arts: NftData[], artsVideo: NftData[], arts3D: NftData[]) {
        this.walletAddress = address
        this.Wearables = Wearables
        this.arts = arts
        this.artsVideo = artsVideo
        this.arts3D = arts3D
        this.emotes=emotes
    }
}
export class OperationResult {
    public problem!: string;
    public iSucceed!: boolean;
    constructor(prob: string, succeed: boolean) {
        this.iSucceed = succeed
        this.problem = prob
    }
}
export class MarketOperationData {
    public nameOfOperation !: string;
    public dataOfNft!: NftData;
    public result!: OperationResult;

    private constructor2(name:string,data:NftData,result:OperationResult) {
        this.nameOfOperation = name
        this.dataOfNft = data
        this.result=result
    };
    private constructor1(data: MarketOperationData) {
        Object.assign(this, data)
    };

  constructor(...attributes: any[]) {
        if (attributes.length === 1) {
            this.constructor1(attributes[0])
            return;
        }
        if (attributes.length === 3) {
            this.constructor2(attributes[0],attributes[1],attributes[2])
            return;
        }
    }
}
export class OrderData {
    public orderName!: string;
    public parameters: string[] = [];
    private constructor2(name: string, parameters: string[]) {
        this.orderName = name
        this.parameters = parameters
    };
    private constructor1(data: OrderData) {
        Object.assign(this, data)
    };
    constructor(...attributes: any[]) {
        if (attributes.length === 1) {
            this.constructor1(attributes[0])
            return;
        }
        if (attributes.length === 2) {
            this.constructor2(attributes[0], attributes[1])
            return;
        }
    }

}