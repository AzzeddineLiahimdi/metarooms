import { OrderModule } from "./order.module";

export class Werabeals {
  public itemId!: string ;
  public contractAddress!: string ;
  public tokenId!:string;

  constructor(token: string, contract: string,item:string) {
    this.itemId = item;
    this.contractAddress = contract;
    this.tokenId=token
  }
}

export class NftData {
  public nft!: Werabeals;
  public isListed!: boolean;
  public price='0';

  constructor(data:any) {
    this.nft = data.nft;
    this.isListed = data.isListed;
    this.price=data.price
  }
}

export class Voucher {
  public tokenId!: string ;
  public contractAddress!: string;
  public tokenOwner: string;
  public price: string;
  public amount: number;
  public amountForSale: number;
  public royalties: any;
  public nonce: any;
  public uri: string;
  public signature: string;

  constructor(data:any,order:OrderModule) {
    this.contractAddress=data.nft.contractAddress,
    this.tokenId= data.nft.tokenId.toString(),
    this.tokenOwner= order.OwnerAddress,
    this.price= order.price.toString(),
    this.amount= 1,
    this.amountForSale= 1,
    this.royalties= order.royalties,
    this.uri= order.metadata,
    this.nonce= order.nonce,
    this.signature= order.signature
  }
}