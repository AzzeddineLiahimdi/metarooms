import { FieldValue, serverTimestamp } from "firebase/firestore";
import { Template } from "./templates";
const timestamp = serverTimestamp;

export class Room {
    id?:string | null;
    OwnerAddress: string;
    InvenAddress:string; 
    TempName:string; 
    jsonDa:string; 
    roomName:string; 
    visitors:number;
    image:string;
    seneName:string;
    category:string;
    description?:string;
    chatId?:string;
    createdAt:FieldValue;
    eventsId!:string;
    constructor(address:string,name:string,description:string,template:Template,chat:string,event:string){
      this.createdAt=timestamp()
      this.InvenAddress=address
      this.OwnerAddress=address
      this.TempName =template.name;
      this.jsonDa = "";
      this.visitors = 0;
      this.roomName=name;
      this.seneName = template.seneName;
      this.image = template.image;
      this.category = template.category;
      this.description=description;
      this.eventsId=event
      this.chatId=chat;
    }
}
