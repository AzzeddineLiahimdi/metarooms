export class Template {
    name!:string;
    seneName!:string;
    image!:string;
    category!:string;
    description!:string;
    collectable=false;
    tokenId!:number;
    amount=1;
    contractAddress!:string;
}
