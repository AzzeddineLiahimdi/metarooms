export class OpenseaObjects {
    public itemId!: string | null;
    public contractAddress!: string | null;
    constructor(token: string, contract: string) {
        this.itemId = token;
        this.contractAddress = contract;
    }
}
