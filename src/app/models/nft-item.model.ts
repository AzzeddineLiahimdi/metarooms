export class NftItem {
  tokenId!: number;
  contract_address!: string;
  amount!: number;
  creator_address!: string;
  metadata!: string;
  isMinted!:boolean;
  onSale!:boolean;
  created!:any;
  standard!:number;
  
  constructor(tokenid:number,contract:string,amount:number,creator:string,metadata:string,standard:number,minted:boolean){
    this.tokenId=tokenid
    this.contract_address=contract
    this.amount=amount
    this.creator_address=creator
    this.metadata=metadata
    this.isMinted=minted
    this.onSale=false
    this.standard=standard
  }
}
