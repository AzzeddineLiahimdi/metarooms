// SPDX-License-Identifier: MIT OR Apache-2.0
pragma solidity ^0.8.4;
pragma abicoder v2; // required to accept structs as function parameters
import "@openzeppelin/contracts/token/ERC1155/ERC1155.sol";
import "@openzeppelin/contracts/utils/cryptography/ECDSA.sol";
import "@openzeppelin/contracts/utils/cryptography/draft-EIP712.sol";
import "@openzeppelin/contracts/security/ReentrancyGuard.sol";
import "@openzeppelin/contracts/utils/Counters.sol";
import "@openzeppelin/contracts/token/ERC721/ERC721.sol";
import "@openzeppelin/contracts/utils/introspection/ERC165Checker.sol";
import "@openzeppelin/contracts/token/common/ERC2981.sol";
import "./AbCollection.sol";
import "./Omnispaces.sol";

contract Marketplace is ReentrancyGuard, EIP712 {
    string private constant SIGNING_DOMAIN = "Omnispaces";
    string private constant SIGNATURE_VERSION = "1";
    address payable private owner;

    using Counters for Counters.Counter;
    mapping(address => Counters.Counter) private _salesOrderNonces;
    mapping(bytes32 => bool) private _invalidateSalesOrders;

    struct NFTVoucher {
        address contractAddress;
        uint256 tokenId;
        address tokenOwner;
        uint256 price;
        uint256 amount;
        uint256 amountForSale;
        uint96 royalties;
        string uri;
        uint256 nonce;
        bytes signature;
    }

    modifier OnlyOwner() {
        require(owner == msg.sender, "Sender not own the contract");
        _;
    }

    constructor() EIP712(SIGNING_DOMAIN, SIGNATURE_VERSION) {
        //set the owner of the contract to the one that deployed it
        owner = payable(msg.sender);
    }

    function getCurrentSalesOrderNonce(address addressOwner)
        public
        view
        returns (uint256)
    {
        return _salesOrderNonces[addressOwner].current();
    }

    function redeem(
        address redeemer,
        address contractAddress,
        uint256 contractType,
        NFTVoucher calldata voucher
    ) public payable nonReentrant returns (uint256) {
        // make sure signature is valid and get the address of the signer
        (bytes32 digest, address signer) = _verify(voucher);
        require(
            msg.value >= voucher.price * voucher.amountForSale,
            "Market: Sales price not achieved"
        );
        require(
            signer == voucher.tokenOwner,
            "Market: Recovered address does not march"
        );
        require(
            _invalidateSalesOrders[digest] != true,
            "Market: invalid order"
        );

        bool isMinted = false;
        if (contractType == 2) {
            isMinted = _isMinted(contractAddress, voucher.tokenId);
            if (!isMinted) {
                //Mint the token to the token owner
                AbCollection(contractAddress).mint(
                    signer,
                    voucher.tokenId,
                    voucher.amount,
                    voucher.uri,
                    voucher.royalties
                );
            } else {
                //Make sure the seller has enough amount of token
                require(
                    ERC1155(contractAddress).balanceOf(
                        signer,
                        voucher.tokenId
                    ) >= voucher.amountForSale,
                    "Market: the seller does not have enough amount of token"
                );
                require(
                    ERC1155(contractAddress).isApprovedForAll(
                        signer,
                        address(this)
                    ),
                    "Market: Market must be approved"
                );
            }

            ERC1155(contractAddress).safeTransferFrom(
                signer,
                redeemer,
                voucher.tokenId,
                voucher.amountForSale,
                ""
            );
        } else {
            isMinted = _isMintedERC721(contractAddress, voucher.tokenId);
            if (!isMinted) {
                Omnispaces(contractAddress).safeMint(
                    signer,
                    voucher.tokenId,
                    voucher.uri,
                    voucher.royalties
                );
            } else {
                //Make sure the seller has enough amount of token
                require(
                    signer == IERC721(contractAddress).ownerOf(voucher.tokenId),
                    "the seller does not have enough amount of token"
                );
                require(
                    IERC721(contractAddress).isApprovedForAll(
                        signer,
                        address(this)
                    ),
                    "Market must be approved"
                );
            }
            IERC721(contractAddress).safeTransferFrom(
                signer,
                redeemer,
                voucher.tokenId,
                ""
            );
        }
        _invalidateSalesOrders[digest] = true;
        _salesOrderNonces[signer].increment();
        _distributeEranings(
            signer,
            voucher.contractAddress,
            voucher.tokenId,
            contractType,
            msg.value
        );
        return voucher.tokenId;
    }

    function _hash(NFTVoucher calldata voucher)
        internal
        view
        returns (bytes32)
    {
        return
            _hashTypedDataV4(
                keccak256(
                    abi.encode(
                        keccak256(
                            "NFTVoucher(address contractAddress,uint256 tokenId,address tokenOwner,uint256 price,uint256 amount,uint256 amountForSale,uint256 royalties,string uri,uint256 nonce)"
                        ),
                        voucher.contractAddress,
                        voucher.tokenId,
                        voucher.tokenOwner,
                        voucher.price,
                        voucher.amount,
                        voucher.amountForSale,
                        voucher.royalties,
                        keccak256(bytes(voucher.uri)),
                        voucher.nonce
                    )
                )
            );
    }

    function getChainID() external view returns (uint256) {
        uint256 id;
        assembly {
            id := chainid()
        }
        return id;
    }

    function _verify(NFTVoucher calldata voucher)
        internal
        view
        returns (bytes32, address)
    {
        bytes32 digest = _hash(voucher);
        address signer = ECDSA.recover(digest, voucher.signature);
        return (digest, signer);
    }

    function _isMinted(address contractAddress, uint256 tokenId)
        internal
        view
        returns (bool)
    {
        //this to get the owner address if it reverts,the token is not minted yet
        try ERC1155(contractAddress).uri(tokenId) returns (
            string memory _value
        ) {
            if (bytes(_value).length > 0) {
                return true;
            } else {
                return false;
            }
        } catch {
            return false;
        }
    }

    function withdraw(address to) public OnlyOwner {
        require(address(this).balance > 0, "Balance is 0");
        payable(to).transfer(address(this).balance);
    }

    function getContractBalance() public view OnlyOwner returns (uint256) {
        return address(this).balance;
    }

    function CancelOrder(NFTVoucher calldata voucher) public {
        // make sure signature is valid and get the address of the signer
        (bytes32 digest, address signer) = _verify(voucher);
        require(msg.sender != signer, "Sender does not own this Order");
        _invalidateSalesOrders[digest] = true;
    }

    function _distributeEranings(
        address seller,
        address contractAddress,
        uint256 tokenId,
        uint256 contractType,
        uint256 earnings
    ) public {
        //share the recived funds between the partes
        uint256 marketShare = (earnings * 125) / 10000;
        (address creator, uint256 creatorShare) = _getCreatorShare(
            contractAddress,
            tokenId,
            seller,
            earnings,
            contractType
        );
        uint256 sellerShare = earnings - marketShare - creatorShare;
        if (creatorShare > 0 && address(0) != creator) {
            payable(creator).transfer(creatorShare);
        }
        payable(seller).transfer(sellerShare);
    }

    function _isMintedERC721(address conntractAddress, uint256 tokenId)
        internal
        view
        returns (bool)
    {
        //try to get Owner address if it reverts the token in not minted yet
        try IERC721(conntractAddress).ownerOf(tokenId) returns (address) {
            return true;
        } catch {
            return false;
        }
    }

    function _getCreatorShare(
        address contractAddress,
        uint256 tokenId,
        address sellerAddress,
        uint256 earnings,
        uint256 contractType
    ) public view returns (address, uint256) {
        // CHECK IF ROYALTY INTERFACE IS SUPPORTED
        if (
            ERC165Checker.supportsInterface(
                contractAddress,
                type(IERC2981).interfaceId
            )
        ) {
            //GET THE CREATOR SHARE
            address creator;
            uint256 creatorShare;
            if (contractType == 1) {
                (creator, creatorShare) = Omnispaces(contractAddress)
                    .royaltyInfo(tokenId, earnings);
            } else {
                (creator, creatorShare) = AbCollection(contractAddress)
                    .royaltyInfo(tokenId, earnings);
            }
            if (creator == sellerAddress) {
                creatorShare = 0;
            }
            return (creator, creatorShare);
        } else {
            return (address(0), 0);
        }
    }
}
