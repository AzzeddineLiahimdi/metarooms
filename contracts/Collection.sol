// SPDX-License-Identifier: MIT
pragma solidity ^0.8.3;

import "./AbCollection.sol";

contract Collection is AbCollection {

constructor(address market, string memory name , string memory  symbol ) AbCollection(market,name,symbol){
}

}
