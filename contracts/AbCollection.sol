// SPDX-License-Identifier: MIT
pragma solidity ^0.8.3;
import "@openzeppelin/contracts/token/ERC1155/ERC1155.sol";
import "@openzeppelin/contracts/access/AccessControl.sol";
import "@openzeppelin/contracts/token/common/ERC2981.sol";
import "@openzeppelin/contracts/utils/Counters.sol";


abstract contract AbCollection is ERC1155,ERC2981, AccessControl {
    bytes32 public constant URI_SETTER_ROLE = keccak256("URI_SETTER_ROLE");
    bytes32 public constant MINTER_ROLE = keccak256("MINTER_ROLE");

    mapping(uint256 => string) public Items; //id => Item Metadata
    string public name;
    string public symbol;
    using Counters for Counters.Counter;
    Counters.Counter private _tokenIds;


    constructor(
        address marketplaceAddress,
        string memory _name,
        string memory _symbol
    ) ERC1155("") {
        //specify the role of each account so that the market is the only one who can mint and transfer the nfts
        _grantRole(DEFAULT_ADMIN_ROLE, msg.sender);
        _grantRole(URI_SETTER_ROLE, msg.sender);
        _grantRole(MINTER_ROLE, msg.sender);
        _grantRole(MINTER_ROLE, marketplaceAddress);
        //specify the name and the symbol of this contract
        name = _name;
        symbol = _symbol;
    }

    function mint(
        address account,
        uint256 id,
        uint256 amount,
        string memory tokenURI,
        uint96 royalties
    ) public onlyRole(MINTER_ROLE) {
        _mint(account, id, amount, "");
        Items[id] = tokenURI;
        _setTokenRoyalty(id, account, royalties);
    }

    function addNewNFT(uint256 initialSupply,string memory tokenURI,uint96 royalties) public returns (uint256){
        _tokenIds.increment();
        uint256 newTokenId = _tokenIds.current();
        mint(msg.sender,newTokenId,initialSupply,tokenURI,royalties);
        return newTokenId;
    }
    
    function setRole(address account) public onlyRole(URI_SETTER_ROLE){
        _grantRole(MINTER_ROLE, account);
    }

    function uri(uint256 tokenid) public view override returns (string memory) {
        return (Items[tokenid]);
    }

    // The following functions are overrides required by Solidity.
    function supportsInterface(bytes4 interfaceId)
        public
        view
        override(ERC1155,ERC2981, AccessControl)
        returns (bool)
    {
        return super.supportsInterface(interfaceId);
    }
}
